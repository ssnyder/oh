/*
    Kinda lame test for the following OH components:
    
    oh::HistogramData

    If you make any bug fixes or other changes to these classes
    it might be a good idea to run/update this test.

    Author unknown =) I think he had blue t-shirt...

*/

#include <string>
#include <vector>
#include <iostream>
#include <oh/core/HistogramData.h>

using namespace std;

int main( void )
{

    bool test = true;

    vector<pair<string,string> > ann1, ann2;
    vector<double> axis1, vector;
    double d1, d2;
    unsigned long ll;
    
    for ( int i = 0; i <= 10; i++ ) axis1.push_back( i );
    ann1.push_back( make_pair( "label 1", "value 1" ) );
    ann1.push_back( make_pair( "label 2", "value 2" ) );
    
    cout << "Testing oh::HistogramData class..." << endl << endl;

    //
    // 1D histogram test
    //
    oh::HistogramData<int> hda;
    string s1, s2;
    // Title test
    hda.set_title( s1 = "Test Text" ); s2 = hda.get_title( );
    test &= ( s1 == s2 );
    assert( test );
    
    // Label test    
    hda.set_axis_title( oh::Axis::X, s1 );
    s2 = hda.get_axis_title( oh::Axis::X );
    test &= ( s1 == s2 );
    assert( test );
    
    // Annotation test
    hda.set_annotations( ann1 );
    hda.get_annotations( ann2 );
    test &= ( ann1 == ann2 );
    assert( test );
    
    // Variable axis test
    hda.set_axis_range( oh::Axis::X, axis1 );
    vector = hda.get_axis_range( oh::Axis::X );
    test &= ( axis1 == vector );
    assert( test );
    test &= ( hda.get_axis_type( oh::Axis::X ) == oh::Axis::Variable );
    assert( test );
    
    // Fixed axis test
    hda.set_axis_range( oh::Axis::X, 0.0, 1.0, 10 );
    hda.get_axis_range( oh::Axis::X, d1, d2 );
    ll = hda.get_bin_count( oh::Axis::X );
    test &= ( d1 == 0.0 && d2 == 1.0 && ll == 10 );
    assert( test );
    test &= ( hda.get_axis_type( oh::Axis::X ) == oh::Axis::Fixed );
    assert( test );
    
    // NoAxis test
    test &= ( hda.get_axis_type( oh::Axis::Y ) == oh::Axis::NoAxis );
    assert( test );
    test &= ( hda.get_axis_type( oh::Axis::Z ) == oh::Axis::NoAxis );
    assert( test );
    
    // Bin content test
    for ( int i = 0; i <= 11; i++ ) {
        hda.set_bin_value( i, 0, 0, i );
    }
    for ( int i = 1; i <= 10; i++ ) {
        test &= ( i == hda.get_bin_value( i, 0, 0 ) );    
    }
    assert( test );
    test &= ( hda.get_bin_value( 0, 0, 0 ) == 0 );
    assert( test );
    test &= ( hda.get_bin_value( 11, 0, 0 ) == 11 );
    assert( test );

    // Bin error test
    for ( int i = 0; i <= 11; i++ ) {
        hda.set_error( i, 0, 0, i );
    }
    
    for ( int i = 1; i <= 10; i++ ) {
        test &= ( i == hda.get_error( i, 0, 0 ) );    
    }
    assert( test );
    
    test &= ( hda.get_error( 0, 0, 0 ) == 0 );
    assert( test );
    test &= ( hda.get_error( 11, 0, 0 ) == 11 );
    assert( test );

    // Number of entires test
    hda.set_entries( 20 );
    test &= ( hda.get_entries( ) == 20 );
    assert( test );
    
    cout << ( test ? "[OK]" : "[FAILED]" ) << endl;
    
    if ( test )
    	return 0;
    else
    	return 1;

}

// EOF
