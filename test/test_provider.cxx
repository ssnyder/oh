/*
    test_provider.cxx
    
    A RAW provider test application.
    
    Serguei Kolos, June 2005

*/

#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>

#include <cmdl/cmdargs.h>
#include <owl/timer.h>
#include <ipc/core.h>
#include <ipc/signal.h>

#include <oh/OHRootProvider.h>

using namespace std;

OHRootProvider * provider;

void stat( double * data, int count, double & mean, double & dev );

CmdArgStr partition_name( 'p', "partition", "partition_name",
                              "Partition name", CmdArg::isREQ );
CmdArgStr server_name( 's', "server", "server_name",
                           "OH (IS) Server name", CmdArg::isREQ );
CmdArgStr provider_name( 'n', "provider", "provider_name",
                              "Histogram provider name", CmdArg::isREQ );
CmdArgStr histogram_name( 'h', "histogram", "histogram_name",
                              "Histogram type name", CmdArg::isREQ );
CmdArgBool	quiet ('q', "quiet", "print only summary lines to standard error.");
CmdArgBool	update('u', "update", "publish histograms with update mod");
CmdArgBool	no_summary ('U', "no-summary", "don't print summary lines to standard error.");
CmdArgInt	scale ('S', "scale", "scale_factor", "bin range multiplication factor.");

typedef char OH_STRING[256];

void set_labels( TAxis * axis )
{
    if ( !axis )
    	return ;

    for ( int i = 1; i < axis->GetNbins(); ++i )
    {
    	char label[32];
        sprintf( label, "label %d", i );
        axis->SetBinLabel( i, label );
    }
}

void setupAxes(TH1 * h, bool add_labels)
{
    h -> GetXaxis( ) -> SetTitle( "X Axis" );
    h -> GetYaxis( ) -> SetTitle( "Y Axis" );
    h -> GetZaxis( ) -> SetTitle( "Z Axis" );
    if ( add_labels )
    {
	set_labels( h -> GetXaxis( ) );
	set_labels( h -> GetYaxis( ) );
	set_labels( h -> GetZaxis( ) );
    }
}

template <class T>
void test( int histogram_number, int iterations, int size, int delay, int fill, int scale )
{
    // Sample data, this should be retrieved from somewhere in a real app
    double * data = new double[ histogram_number * iterations ];
    double   tmin = 1000000.;
    double   tmax = 0.;
    int	    errcnt = 0;
    int     count = 0;
    
    OH_STRING * names = new OH_STRING[histogram_number];
    TH1 ** histograms = new TH1*[histogram_number];
    srand(1);
    size *= 2;
    int bins = 0;
    for( int n = 0; n < histogram_number; n++ )
    {
    	sprintf(names[n],"%s.%d",(const char*)histogram_name,n);
        int N = (rand() % size + 1) * scale;
        histograms[n] = new T( names[n], names[n], N, 0, 1 );
	if ( !fill )
        {
            histograms[n]->SetBinContent( 1, n + 1 );
        }
        else
        {
           histograms[n]->FillRandom( "gaus", rand() % size + 1 );
        }
	setupAxes(histograms[n], true);
        bins += N;
    }
    std::cout << "total number of bins is " << bins << std::endl;
    
    for( int i = 0; i < iterations; i++ )
    {
	for( int n = 0; n < histogram_number; n++ )
	{
	    OWLTimer t;
	    t.start();
            if ( !quiet )
            	std::cout << provider_name << ": publishing '" << names[n] << "' histogram" << std::endl;
            provider->publish( *histograms[n], names[n] );
	    t.stop();
	    data[count++] = t.totalTime();

	    tmin = ( tmin < t.totalTime() ) ? tmin : t.totalTime();
	    tmax = ( tmax > t.totalTime() ) ? tmax : t.totalTime();

	    if ( !quiet )
		std::cout << count << "\t" << t.totalTime() * 1000 << std::endl;
	}
	if ( delay > 0 )
	    sleep( delay );
    }
        
    if ( !no_summary )
    {
    	double mean, dev;
	
	stat( data, histogram_number * iterations, mean, dev );
	std::cerr << "IS test finished (pid = " << getpid() << ") : " ;
	std::cerr << count << " publish done, " << errcnt << " publish failed : " << std::endl;
	
	std::cerr << "{ min/avg/max/dev = " << tmin * 1000. << "/"
				       << mean * 1000. << "/" 
				       << tmax * 1000. << "/" 
				       << dev  * 1000. << std::endl;
    }

    for( int n = 0; n < histogram_number; n++ )
    {
        delete histograms[n];
    }

    delete[] data;
    delete[] names;
    delete[] histograms;
}

void stat( double * data, int count, double & mean, double & dev )
{
    int i;
    mean = 0.;

    for ( i = 0; i < count; i++ )
    {
	mean += data[i];
    }
    mean /= count;

    dev = 0.;

    for ( i = 0; i < count; i++ )
    {
	dev += ( data[i] - mean ) * ( data[i] - mean );
    }
    dev /= count;

    dev = sqrt( dev );
}

class MyCommandListener : public OHCommandListener
{
  public:
    void command ( const std::string & name, const std::string & cmd )
    {
	std::cout << " Command " << cmd << " received for the " << name << " histogram" << std::endl;
    }

    void command ( const std::string & cmd )
    {
	std::cout << " Command " << cmd << " received for all histograms" << std::endl;
	if ( cmd == "exit" )
	{
	    daq::ipc::signal::raise();
	}
        else
        {
            char type;
            int hnum, rnum, size, delay, fill;
            sscanf( cmd.c_str(), "%c %d %d %d %d %d", &type, &hnum, &rnum, &size, &delay, &fill );
            std::cerr << "Publishing " << hnum << " histograms of type " << type 
	    		<< " of size " << size << " " 
	    		<< rnum << " times with " << delay << " seconds delay" << std::endl;
            switch ( type )
            {
            	case 'S':
		    test<TH1S>( hnum, rnum, size, delay, fill, scale );
		    break;
            	case 'I':
		    test<TH1I>( hnum, rnum, size, delay, fill, scale );
		    break;
            	case 'F':
		    test<TH1F>( hnum, rnum, size, delay, fill, scale );
		    break;
            	case 'D':
		    test<TH1D>( hnum, rnum, size, delay, fill, scale );
		    break;
                default:
                    std::cerr << "Bad command " << cmd << " has wrong type " << type << std::endl;
	    }
        }
    }
};

int main( int argc, char ** argv )
{
    IPCCore::init( argc, argv );
    
    CmdLine cmd( *argv, &partition_name, &server_name,
                 &provider_name, &histogram_name, &update, &quiet, &no_summary, &scale, NULL );
    
    scale = 1;
    
    CmdArgvIter arg_iter( --argc, ++argv );
    
    cmd.description( "OH RAW provider example" );
        
    cmd.parse(arg_iter);
    
    //
    // Create an OHRawProvider and publish some sample histograms
    /////////////////////////////////////////////////////////////////////////
    
    IPCPartition partition( partition_name );
    MyCommandListener lst;
    try
    {
    	provider = new OHRootProvider( partition, (const char*)server_name, (const char*)provider_name, &lst );
    }
    catch( daq::oh::Exception & ex )
    {
    	ers::fatal( ex );
        return 1;
    }
                		 
    std::cout << "OH test provider \"" << provider_name << "\" has been started in the \"" 
    		<< partition_name << "\" partition" << std::endl;
                
    daq::ipc::signal::wait_for();
    return 0;
}

