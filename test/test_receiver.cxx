/*
    test_receiver.cxx
    
    A simple utility which subscribes for histograms
    
    Serguei Kolos, June 2005

*/

#include <iostream>
using namespace std;

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/signal.h>
#include <owl/regexp.h>

// OH include files
#include <oh/OHRootReceiver.h>
#include <oh/OHSubscriber.h>

/*
    This is the histogram receiver implementation. It simply
    displays the received histograms on the screen. All histograms
    retrieved with a OHHistogramIterator or OHSubscriber
    finally must end up in some receiver implementation.
*/
class MyReceiver : public OHRootReceiver {
public:
    MyReceiver( int cnt )
      : counter_( 0 ),
	output_counter_( cnt )
    { ; }
    
    void receive( OHRootHistogram & )
    {
    	if ( ++counter_%output_counter_ == 0 )
        {
            std::cout << counter_ << " notifications have been received" << std::endl;
        }
    }
    
    void receive( vector<OHRootHistogram*> & )
    {
    	if ( ++counter_%output_counter_ == 0 )
        {
            std::cout << counter_ << " notifications have been received" << std::endl;
        }
    }
    
    void histogramChanged( const std::string & , const OWLTime & , OHReceiver::Reason )
    {
    	if ( ++counter_%output_counter_ == 0 )
        {
            std::cout << counter_ << " notifications have been received" << std::endl;
        }
    }
    
    void receive( OHRootGraph & )
    {
    	if ( ++counter_%output_counter_ == 0 )
        {
            std::cout << counter_ << " notifications have been received" << std::endl;
        }
    }
    
    void receive( vector<OHRootGraph*> & )
    {
    	if ( ++counter_%output_counter_ == 0 )
        {
            std::cout << counter_ << " notifications have been received" << std::endl;
        }
    }
    
    void receive( OHRootGraph2D & )
    {
    	if ( ++counter_%output_counter_ == 0 )
        {
            std::cout << counter_ << " notifications have been received" << std::endl;
        }
    }
    
    void receive( vector<OHRootGraph2D*> & )
    {
    	if ( ++counter_%output_counter_ == 0 )
        {
            std::cout << counter_ << " notifications have been received" << std::endl;
        }
    }
    
private:
    int counter_;
    int output_counter_;
};

int main( int argc, char ** argv )
{

    IPCCore::init( argc, argv );
        
    //
    // Get parameters from the command line
    //
    CmdArgStr	partition_name( 'p', "partition", "partition_name",
                              "Partition name", CmdArg::isREQ );
    CmdArgStr	server_name( 's', "server", "server_name",
                           "OH (IS) Server name", CmdArg::isREQ );
    CmdArgStr	provider_name( 'n', "provider", "provider_name",
                             "Histogram provider name" );
    CmdArgStr	histogram_name( 'h', "histogram", "histogram_name",
                              "Histogram type name" );
    CmdArgInt	iters('i', "iters", "iterations", "number of iterations to report");
    
    CmdArgBool	value ('v', "value", "subscribe for receiving histogram values (default off).");
    
    CmdLine cmd( *argv, &partition_name, &server_name,
                 &provider_name, &histogram_name, &iters, &value, NULL );
    
    iters = 1000;
    value = false;
    
    CmdArgvIter arg_iter( --argc, ++argv );
    
    cmd.description( "OH Histogram display utility" );
    
    cmd.parse(arg_iter);
    
    //
    // Check command line parameters
    if( provider_name.isNULL( ) ) provider_name = ".*";
    if( histogram_name.isNULL( ) ) histogram_name = ".*";
    
    try
    {
	MyReceiver receiver( iters );
	IPCPartition p( partition_name );

	OHSubscriber ohhs( p, (const char*)server_name, receiver, false );

	OWLRegexp provider(provider_name);
	OWLRegexp histogram(histogram_name);
	ohhs.subscribe( provider, histogram, value );

	std::cout << "OH test receiver for the \"" << provider_name << ":" << histogram_name <<
		    "\" has been started in the \"" << partition_name << "\" partition" << std::endl;

	daq::ipc::signal::wait_for();
    }
    catch ( daq::oh::Exception & ex )
    {
    	ers::fatal( ex );
        return 1;
    }
    
    return 0;
}

