/*
    oh::util.cxx
    
    Fredrik Sandin, Oct 2001
    Monika Barczyk, Feb 2003
    Sergei Kolos, Jan 2004
*/

#include <oh/OHUtil.h>

namespace
{
    const std::string empty_string("");
}

std::string 
oh::util::get_server_name ( const std::string & s )
{
    std::string::size_type start_pos = s.find( "." );
    if ( start_pos != std::string::npos )
    {
        return s.substr( 0, start_pos );
    }

    return empty_string;
}

std::string 
oh::util::get_provider_name ( const std::string & s )
{
    std::string::size_type start_pos = s.find( '.' );
    if ( start_pos == std::string::npos )
    {
    	// This is the name of a special provider object
        return s;
    }
    else
    {
	std::string::size_type end_pos = s.find( '.', start_pos + 1 );
	if ( start_pos != end_pos )
	{
	    return s.substr( start_pos + 1, end_pos - start_pos - 1 );
	}
    }
    return empty_string;
}

std::string
oh::util::get_object_name( const std::string & s )
{
    std::string::size_type start_pos = s.find( '.' );
    if ( start_pos == std::string::npos )
    {
	return empty_string;
    }
    start_pos = s.find( '.', start_pos + 1 );
    if ( start_pos != std::string::npos )
    {
    	return s.substr( start_pos + 1 );
    }
    return empty_string;
}

std::string
oh::util::get_histogram_name( const std::string & s )
{
    return get_object_name( s );
}

std::string
oh::util::create_info_name (	const std::string & server_name, 
				const std::string & provider_name, 
				const std::string& histo_name )
{
    static const std::string separator = ".";
    return ( server_name + separator + provider_name + separator + histo_name );
}

std::string
oh::util::create_regex (	const std::string & provider_name, 
				const std::string& histo_name )
{
    static const std::string separator = "\\.";
    return ( provider_name + separator + histo_name );
}

std::string 
oh::util::create_provider_name (	const std::string & server_name, 
					const std::string & provider_name )
{
    std::string sname = server_name;
    if ( !sname.empty() )
    	sname += '.';
    return ( sname + provider_name );
}

// EOF
