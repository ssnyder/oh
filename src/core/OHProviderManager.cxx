/*
    OHProvider.cxx

    Fredrik Sandin, Oct 2001
    Monika Barczyk, Sept 2002
    Serguei Kolos, Jan 2004
*/
#include <algorithm>
#include <functional>

#include <is/infoT.h>
#include <oh/internal/OHProviderManager.h>
#include <oh/OHUtil.h>
#include <oh/core/HistogramProvider.h>

/*!
    \param p A valid IPCPartition
    \param server Name of a valid OH server (An IS server in partition p)
    \param name Name of the provider, user is responsible for specifying a
     		unique name. Should only contain characters from [a-z], [A-Z], [0-9] and '_'.
*/
oh::provider::provider( const IPCPartition & p,
			const std::string & server,
			const std::string & name )
  : dictionary_( p ),
    name_( oh::util::create_provider_name( server, name ) ),
    alarm_( 60., std::bind( &oh::provider::checkAndRepublish, this ) )
{
    // Add the provider name to the IS server, success will imply
    // that the provider has a unique name within this server. This
    // information is also used by the OHProviderIterator class to
    // retrieve active providers.
    
    oh::HistogramProvider providerinfo;
    providerinfo.active = true;
    try
    {
    	dictionary_.getValue( name_, providerinfo );
	if ( providerinfo.active && providerinfo.providerExist() )
	{
	    throw daq::oh::ProviderAlreadyExist( ERS_HERE, name, server );
	}
	else
	{
	    providerinfo.active = true;
	    dictionary_.update( name_, providerinfo );
	}
    }
    catch ( daq::is::InfoNotCompatible & ex )
    {
	throw daq::oh::ProviderAlreadyExist( ERS_HERE, name, server, ex );
    }
    catch ( daq::is::RepositoryNotFound & ex )
    {
	throw daq::oh::RepositoryNotFound( ERS_HERE, server, ex );
    }
    catch ( daq::is::InfoNotFound & ex )
    {
	try
        {
            dictionary_.insert( name_, providerinfo );
        }
        catch ( daq::is::InfoAlreadyExist & ex )
        {
	    throw daq::oh::ProviderAlreadyExist( ERS_HERE, server, name );
        }
	catch ( daq::is::RepositoryNotFound & ex )
	{
	    throw daq::oh::RepositoryNotFound( ERS_HERE, server, ex );
	}
    }
}

oh::provider::~provider( )
{
    oh::HistogramProvider providerinfo;
    providerinfo.active = false;
    try
    {
    	dictionary_.update( name_, providerinfo );
    }
    catch ( daq::is::Exception & ex )
    {
    	ers::debug( ex, 0 );
    }
}

bool
oh::provider::checkAndRepublish()
{
    ERS_DEBUG( 1, "Check if provider is properly published in IS and re publish if necessary" );
    try
    {
    	if ( !dictionary_.contains( name_ ) )
        {
	    oh::HistogramProvider providerinfo;
	    providerinfo.active = active();
            dictionary_.checkin( name_, providerinfo );
	    ERS_DEBUG( 1, "Provider '" << name_ << "' was successfully re published to IS" );
        }
    }
    catch ( daq::is::Exception & ex )
    {
    	ERS_DEBUG( 1, "Cannot re publish '" << name_ << " OH provider: " << ex.what() );
    }
    
    return true;
}

void
oh::provider::addListener( OHCommandListener * oh_lst, ISCommandListener * is_lst )
{
    std::scoped_lock lock( mutex_ );
    Listeners::iterator it;
    it = std::find( listeners_.begin(), listeners_.end(), listener( oh_lst ) );
    if ( it == listeners_.end() )
    {
	ISInfoProvider::instance().addCommandListener( is_lst );
	listeners_.push_back( listener( oh_lst, is_lst, true ) );
    }
    else
    {
	listeners_.push_back( listener( oh_lst, is_lst, false ) );
    }
}

void
oh::provider::removeListener( OHCommandListener * oh_lst, ISCommandListener * is_lst )
{
    std::scoped_lock lock( mutex_ );
    Listeners::iterator it;
    it = std::find( listeners_.begin(), listeners_.end(), listener( oh_lst, is_lst ) );
    if ( it != listeners_.end() )
    {
        listener tmp = *it;
        listeners_.erase( it );
        if ( tmp.active )
        {
            ISInfoProvider::instance().removeCommandListener( is_lst );
	    it = std::find( listeners_.begin(), listeners_.end(), listener( oh_lst ) );
	    if ( it != listeners_.end() )
	    {
		it->active = true;
                ISInfoProvider::instance().addCommandListener( it->isl );
	    }
        }
    }
}

/////////////////////////////////////////////////////////////////////////
// oh::provider_manager
/////////////////////////////////////////////////////////////////////////

void
oh::provider_manager::registerProvider(	const IPCPartition & p,
					const std::string & server,
                                        const std::string & name,
					OHCommandListener * oh_lst,
                                        ISCommandListener * is_lst )
{
    std::string key = p.name() + server + name;
    
    std::scoped_lock lock( mutex_ );
    Providers::iterator it = providers_.find(key);
    if ( it == providers_.end() )
    {
        it = providers_.insert( std::make_pair( key, new provider( p, server, name ) ) ).first;
    }
    it->second -> addListener( oh_lst, is_lst );
}

void
oh::provider_manager::unregisterProvider( const IPCPartition & p,
					 const std::string & server,
                                         const std::string & name,
					 OHCommandListener * oh_lst,
                                         ISCommandListener * is_lst )
{
    std::string key = p.name() + server + name;
    
    std::scoped_lock lock( mutex_ );
    Providers::iterator it = providers_.find(key);
    if ( it != providers_.end() )
    {
	it->second -> removeListener( oh_lst, is_lst );
        if (!it->second->active()) {
	    delete it->second;
	    providers_.erase(it);
        }
    }
}

void
oh::provider_manager::destroy( )
{
    std::scoped_lock lock( mutex_ );
    ERS_DEBUG( 1, "provider manager is destroyed" );
    std::map<std::string,provider*>::iterator it = providers_.begin( );
    while ( it != providers_.end( ) )
    {
	delete it->second;
	providers_.erase(it++);
    }
}
