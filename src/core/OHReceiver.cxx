#include <oh/core/ProfileData.h>
#include <oh/interface/OHReceiver.h>
#include <oh/OHUtil.h>
#include <oh/exceptions.h>
#include <oh/core/DataTypes.h>

#define CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE( _TT_ ) \
    if ( infoany.type() == _TT_::type() ) { \
        _TT_ info; \
        infoany >>= info; \
	receive_( name, info ); \
    } else

#define CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE_V( _TT_ ) \
    if ( infoany_vector[0].type() == _TT_::type() ) { \
	std::vector<_TT_> info_vector( infoany_vector.size() ); \
	for ( size_t i = 0; i < infoany_vector.size(); ++i ) \
            infoany_vector[i] >>= info_vector[i]; \
	receive_( name, info_vector ); \
    } else


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	PRIVATE METHODS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       
void
OHReceiver::readObjects( const IPCPartition & partition, const std::string & name, int how_many )
{
    ISInfoDictionary dictionary( partition );
    if ( how_many != 1 )
    {
	std::vector<ISInfoAny> infoany_vector;
	dictionary.getValues( name, infoany_vector, how_many );
	if ( removePolicy() ) {
	    dictionary.remove( name );
	}
	if ( infoany_vector.size() ) {
	    FOR_OH_ALL_TYPES( CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE_V )
            {
            	throw daq::oh::InvalidObject(ERS_HERE);
            }
	}
    }
    else
    {
	ISInfoAny infoany;
	dictionary.getValue( name, infoany );
	if ( removePolicy() ) {
	    dictionary.remove( name );
	}
	FOR_OH_ALL_TYPES( CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE )
        {
	    throw daq::oh::InvalidObject(ERS_HERE);
        }
    }
}

void
OHReceiver::readObjectWithTag( const IPCPartition & partition, const std::string & name, int tag )
{
    ISInfoDictionary dictionary( partition );
    
    ISInfoAny infoany;
    dictionary.getValue( name, tag, infoany );
    if ( removePolicy() ) {
	dictionary.remove( name );
    }
    FOR_OH_ALL_TYPES( CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE )
    {
	throw daq::oh::InvalidObject(ERS_HERE);
    }
}

void
OHReceiver::readObjects( const ISInfoIterator & iterator, bool with_history, short how_many )
{    
    const std::string name = iterator.name();
    
    if ( with_history )
    {
	std::vector<ISInfoAny> infoany_vector;
	iterator.values( infoany_vector, how_many );
	if ( removePolicy() ) {
	    ISInfoDictionary dictionary( iterator.partition() );
            dictionary.remove( iterator.name() );
	}
        
	if ( infoany_vector.size() ) {
	    FOR_OH_ALL_TYPES( CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE_V )
	    {
		throw daq::oh::InvalidObject(ERS_HERE);
	    }
	}
    }
    else
    {
	ISInfoAny infoany;
	iterator.value( infoany );
	if ( removePolicy() ) {
	    ISInfoDictionary dictionary( iterator.partition() );
	    dictionary.remove( iterator.name() );
	}
	FOR_OH_ALL_TYPES( CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE )
        {
	    throw daq::oh::InvalidObject(ERS_HERE);
        }
    }
}

void
OHReceiver::readObjectWithTag( const ISInfoIterator & iterator, int tag )
{    
    ISInfoAny infoany;
    iterator.value( tag, infoany );
    if ( removePolicy() ) {
	ISInfoDictionary dictionary( iterator.partition() );
	dictionary.remove( iterator.name() );
    }
    std::string name = iterator.name();
    FOR_OH_ALL_TYPES( CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE )
    {
	throw daq::oh::InvalidObject(ERS_HERE);
    }
}

void
OHReceiver::readObject( ISInfoStream & stream )
{    
    std::string name = stream.name();
    
    ISInfoAny infoany;
    stream >> infoany;
    FOR_OH_ALL_TYPES( CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE )
    {
	throw daq::oh::InvalidObject(ERS_HERE);
    }
}

void
OHReceiver::readObject( const ISCallbackInfo & callback )
{
    ISInfoAny infoany;
    callback.value( infoany );
    std::string name = callback.name();
    FOR_OH_ALL_TYPES( CONVERT_TO_ACTUAL_TYPE_AND_CALL_RECEIVE )
    {
	throw daq::oh::InvalidObject(ERS_HERE);
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	PUBLIC METHODS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*!
    Call this function to get all tags for a particular object from the OH repository.
    If object is not found throws the daq::oh::ObjectNotFound exception.
    \param partition TDAQ IPC partition
    \param server OH server name
    \param provider name of the histogram provider
    \param histoname name of the histogram
    \param tags vector of <tag:time> pairs which will be filled by this function in case of success. 
    \sa getHistogram
*/
void
OHReceiver::getTags(	const IPCPartition & partition,
		    	const std::string & server,
		    	const std::string & provider,
		    	const std::string & histoname,
			std::vector<std::pair<int,OWLTime> > & tags ) 
{
    
    return getTags( partition, oh::util::create_info_name( server, provider, histoname ), tags );
}

/*!
    Call this function to get all tags for a particular object from the OH repository.
    If object is not found throws the daq::oh::ObjectNotFound exception.
    \param partition TDAQ IPC partition
    \param name full object name which includes dot separated server name, provider name and object name
    \param tags vector of <tag:time> pairs which will be filled by this function in case of success. 
    \sa getHistogram
*/
void
OHReceiver::getTags(	const IPCPartition & partition,
		    	const std::string & name,
			std::vector<std::pair<int,OWLTime> > & tags ) 
{
    ISInfoDictionary	dictionary( partition );
    try {
    	dictionary.getTags( name, tags );
    }
    catch( daq::is::Exception & ex ) {
    	throw daq::oh::ObjectNotFound( ERS_HERE, oh::util::get_server_name( name ),
        					 oh::util::get_provider_name( name ), 
        					 oh::util::get_object_name( name ), 
                                                 0, ex );
    }
}

/*!
    Call this function to get a particular histogram from the OH repository.
    One of the two "receive" functions will be called and requested histogram 
    will be passed to it. If histogram is not found throws the daq::oh::ObjectNotFound
    exception. If object is not supported by the OH throws daq::oh::InvalidObject exception.
    \param partition TDAQ IPC partition
    \param server OH server name
    \param provider name of the histogram provider
    \param histoname name of the histogram
    \param with_history set it to true if you want to retrieve a vector of old 
    values for this histogram. By default is set to false.
    \param how_many Used in combination with the <i>with_history</i> parameter.
    Tells how many history values to retrieve. Default value is -1, which means 
    all values. 
*/
void 
OHReceiver::getHistogram(	const IPCPartition & partition,
				const std::string & server,
				const std::string & provider,
				const std::string & histoname,
				bool  with_history,
				int   how_many ) 
{
    const std::string name = oh::util::create_info_name( server, provider, histoname );
    readObjects( partition, name, with_history ? how_many : 1 );
}

/*!
    Call this function to get a particular histogram from the OH repository.
    One of the two "receive" functions will be called and requested histogram 
    will be passed to it. If histogram is not found throws the daq::oh::ObjectNotFound
    exception. If object is not supported by the OH throws daq::oh::InvalidObject exception.
    \param partition TDAQ IPC partition
    \param name full histogram name which includes dot separated server name, provider name and histogram name
    \param with_history set it to true if you want to retrieve a vector of old 
    values for this histogram. By default is set to false.
    \param how_many Used in combination with the <i>with_history</i> parameter.
    Tells how many history values to retrieve. Default value is -1, which means 
    all values. 
*/
void 
OHReceiver::getHistogram(	const IPCPartition & partition,
				const std::string & name,
				bool  with_history,
				int   how_many ) 
{
    readObjects( partition, name, with_history ? how_many : 1 );
}

/*!
    Call this function to get a particular histogram from the OH repository.
    One of the two "receive" functions will be called and requested histogram 
    will be passed to it. If histogram is not found throws the daq::oh::ObjectNotFound
    exception. If object is not supported by the OH throws daq::oh::InvalidObject exception.
    \param partition TDAQ IPC partition
    \param server OH server name
    \param provider name of the histogram provider
    \param histoname name of the histogram
    \param tag hitograms tag
*/
void 
OHReceiver::getHistogram(	const IPCPartition & partition,
				const std::string & server,
				const std::string & provider,
				const std::string & histoname,
				int tag ) 
{
    const std::string name = oh::util::create_info_name( server, provider, histoname );
    readObjectWithTag( partition, name, tag );
}

/*!
    Call this function to get a particular histogram from the OH repository.
    One of the two "receive" functions will be called and requested histogram 
    will be passed to it. If histogram is not found throws the daq::oh::ObjectNotFound
    exception. If object is not supported by the OH throws daq::oh::InvalidObject exception.
    \param partition TDAQ IPC partition
    \param name full histogram name which includes dot separated server name, provider name and histogram name
    \param tag hitograms tag
*/
void 
OHReceiver::getHistogram(	const IPCPartition & partition,
				const std::string & name,
				int tag ) 
{
    readObjectWithTag( partition, name, tag );
}

/*!
    Call this function to get a particular histogram or graph from the OH repository.
    One of the two "receive" functions will be called and requested histogram 
    will be passed to it. If histogram is not found throws the daq::oh::ObjectNotFound
    exception. If object is not supported by the OH throws daq::oh::InvalidObject exception.
    \param partition TDAQ IPC partition
    \param server OH server name
    \param provider name of the object provider
    \param histoname name of the object
    \param with_history set it to true if you want to retrieve a vector of old 
    values for this object. By default is set to false.
    \param how_many Used in combination with the <i>with_history</i> parameter.
    Tells how many history values to retrieve. Default value is -1, which means 
    all values. 
*/
void 
OHReceiver::getObject(	const IPCPartition & partition,
			const std::string & server,
			const std::string & provider,
			const std::string & histoname,
			bool  with_history,
			int   how_many ) 
{
    const std::string name = oh::util::create_info_name( server, provider, histoname );
    readObjects( partition, name, with_history ? how_many : 1 );
}

/*!
    Call this function to get a particular histogram or graph from the OH repository.
    One of the two "receive" functions will be called and requested histogram 
    will be passed to it. If histogram is not found throws the daq::oh::ObjectNotFound
    exception. If object is not supported by the OH throws daq::oh::InvalidObject exception.
    \param partition TDAQ IPC partition
    \param name full object name which includes dot separated server name, provider name and object name
    \param with_history set it to true if you want to retrieve a vector of old 
    values for this object. By default is set to false.
    \param how_many Used in combination with the <i>with_history</i> parameter.
    Tells how many history values to retrieve. Default value is -1, which means 
    all values. 
*/
void 
OHReceiver::getObject(	const IPCPartition & partition,
			const std::string & name,
			bool  with_history,
			int   how_many ) 
{
    readObjects( partition, name, with_history ? how_many : 1 );
}

/*!
    Call this function to get a particular histogram or graph from the OH repository.
    One of the two "receive" functions will be called and requested histogram 
    will be passed to it. If histogram is not found throws the daq::oh::ObjectNotFound
    exception. If object is not supported by the OH throws daq::oh::InvalidObject exception.
    \param partition TDAQ IPC partition
    \param server OH server name
    \param provider name of the object provider
    \param histoname name of the object
    \param tag object's tag
*/
void 
OHReceiver::getObject(	const IPCPartition & partition,
			const std::string & server,
			const std::string & provider,
			const std::string & histoname,
			int tag ) 
{
    const std::string name = oh::util::create_info_name( server, provider, histoname );
    readObjectWithTag( partition, name, tag );
}

/*!
    Call this function to get a particular histogram or graph from the OH repository.
    One of the two "receive" functions will be called and requested histogram 
    will be passed to it. If histogram is not found throws the daq::oh::ObjectNotFound
    exception. If object is not supported by the OH throws daq::oh::InvalidObject exception.
    \param partition TDAQ IPC partition
    \param name full object name which includes dot separated server name, provider name and object name
    \param tag object's tag
*/
void 
OHReceiver::getObject(	const IPCPartition & partition,
			const std::string & name,
			int tag ) 
{
    readObjectWithTag( partition, name, tag );
}

/*  Override this function if you want to subscribe for OH repository changes.
    Modified objects themselves are not transported from the OH to the subscriber. 
    It has to be used if object values are not necessary in order to gain 
    some performance. This method is called when a special parameter is used 
    for the OHHistogramSubscriber::subscribe methods.

    \param name A histogram name.
    \param time Time when the hsitogram has been changed. 
    \param reason Explains what has happened with the histogram.
    The possibilities are: OHHistogram::Created, OHHistogram::Updated, OHHistogram::Deleted.
    
    \sa OHHistogramSubscriber
*/
void
OHReceiver::objectChanged( const std::string & , const OWLTime & , Reason )
{
}
