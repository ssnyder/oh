#include <oh/core/HistogramData.h>

template <> std::string oh::HistogramData<char>::ClassName("HistogramData<char>");
template <> std::string oh::HistogramData<short>::ClassName("HistogramData<short>");
template <> std::string oh::HistogramData<int>::ClassName("HistogramData<int>");
template <> std::string oh::HistogramData<float>::ClassName("HistogramData<float>");
template <> std::string oh::HistogramData<double>::ClassName("HistogramData<double>");
