/*
    OHProvider.cxx

    Fredrik Sandin, Oct 2001
    Monika Barczyk, Sept 2002
    Serguei Kolos, Jan 2004
*/

#include <iostream>

#include <is/infodictionary.h>

#include <oh/interface/OHProvider.h>
#include <oh/internal/OHProviderManager.h>

#include <oh/OHUtil.h>

using std::string;

/*!
    \param p A valid IPCPartition
    \param server Name of a valid OH server (An IS server in partition p)
    \param name Name of the provider, user is responsible for specifying a
    unique name. Should only contain characters from [a-z], [A-Z], [0-9]
    and '_'.
*/
OHProvider::OHProvider( const IPCPartition & p,
			const string & server,
			const string & name,
			OHCommandListener * lst )
  : partition_( p ),
    server_( server ),
    name_( name ),
    listener_( lst )
{
    OHProviderManager::instance().registerProvider( partition_, server_, name_, listener_, this );
    ERS_DEBUG( 1, "Provider \"" << name_ << "\" for the \"" 
    			<< server_ << "\" server in partition \"" 
                        << partition_.name( ) << "\" was created" );
}

OHProvider::~OHProvider( )
{
    OHProviderManager::instance().unregisterProvider( partition_, server_, name_, listener_, this );
    ERS_DEBUG( 1, "Provider \"" << name_ << "\" for the \"" 
    			<< server_ << "\" server in partition \"" 
                        << partition_.name( ) << "\" was destroyed" );
}

/*!
    \param object A valid histogram or graph object
    \param name A name describing the histogram. Should only contain characters
    from [a-z], [A-Z], [0-9] and '_'.
    \param tag A flag which will be associated with this version of the histogram
    \return CommFailure if a communication error occurred, InvalidArgument if
    one or more arguments is invalid else Success.
*/
void
OHProvider::publish( ISInfo & object, const std::string & hn, int tag ) const
{
    std::string name = oh::util::create_info_name( server_, name_, hn );
    
    try
    {
        ISInfoDictionary( partition_ ).checkin( name, tag, object );
    }
    catch( daq::is::InfoNotCompatible & ex )
    {
    	throw daq::oh::ObjectTypeMismatch( ERS_HERE, name, ex );
    }
    catch( daq::is::RepositoryNotFound & ex )
    {
    	throw daq::oh::RepositoryNotFound( ERS_HERE, server_, ex );
    }
}

/*!
    \param object A valid histogram or graph object
    \param name A name describing the histogram. Should only contain characters
    from [a-z], [A-Z], [0-9] and '_'.
    \return CommFailure if a communication error occurred, InvalidArgument if
    one or more arguments is invalid else Success.
*/
void
OHProvider::publish( ISInfo & object, const std::string & hn ) const
{
    std::string name = oh::util::create_info_name( server_, name_, hn );
    
    try
    {
        ISInfoDictionary( partition_ ).checkin( name, object );
    }
    catch( daq::is::InfoNotCompatible & ex )
    {
    	throw daq::oh::ObjectTypeMismatch( ERS_HERE, name, ex );
    }
    catch( daq::is::RepositoryNotFound & ex )
    {
    	throw daq::oh::RepositoryNotFound( ERS_HERE, server_, ex );
    }
}

void
OHProvider::command( const std::string & name, const std::string & cmd )
{    
    ERS_DEBUG( 3, "Command \"" << cmd << "\" is received for the \"" << name << "\" object" );
    if ( !listener_ )
    	return;
    
    string mask_h = oh::util::create_info_name( server_, name_, string() );
    string mask_p = oh::util::create_provider_name( server_, name_ );
    if ( name.find( mask_h ) == 0 )
    {
	ERS_DEBUG( 3, "Command \"" << cmd << "\" is forwarded to the \"" << name << "\" histogram" );
    	listener_->command( oh::util::get_object_name( name ), cmd );
    }
    else if ( name.find( mask_p ) == 0 )
    {
	ERS_DEBUG( 3, "Command \"" << cmd << "\" is forwarded to the \"" << name_ << "\" provider" );
    	listener_->command( cmd );
    }
}
