/**
    \page main Online Histogramming User's Manual
    
    The Online Histogramming (OH) subsystem allows user histogram tasks
    to retreive histograms from a number of histogram providers.
    Support for graphs has been added, which is provided by the same
    interface as is for histograms.
    
    Users should use the OHServerIterator class to retreive information about
    available OH servers in a partition. Optionally the OHProviderIterator class
    can be used to retreive information about currently active histogram
    providers. To search for available histograms and to retreive existing
    histograms the OHHistogramIterator class should be used. If the user wants
    to subscribe for histograms which have not yet been produced the
    OHHistogramSubscriber class should be used.
    
    In order to receive a histogram from the OH, either by using an
    OHIterator or an OHSubscriber, the user must have
    a receiver interface whch translates the histograms into an understandable
    format. Currently the following receiver interfaces are available
    - OHRootReceiver

    In order to publish histograms in the OH the user must have a provider
    interface which can translate the histograms into a format which is
    understood by the OH. Currently the following provider interfaces are
    available
    - OHRawProvider
    - OHRootProvider
*/
