/* 
   OHFolder.cxx
   
   Monika Barczyk & Piotr Golonka, May 2002
   Serguei Kolos, Jan 2004
*/
#include <TGMsgBox.h>

#include "OHFolder.h"
#include "OHObject.h"
#include "OHProvider.h"
#include "OHPartition.h"
#include "OHRootBrowser.h"
#include "OHConfigDialog.h"

#include <oh/OHFileSaver.h>
#include <oh/OHIterator.h>
#include <oh/OHCommandSender.h>

const TGPicture * 
OHFolder::GetSmallPicture() const
{
    static const TGPicture * picture = gClient->GetPicture("folder__16x16");
    return picture;
}
  
const TGPicture * 
OHFolder::GetLargePicture() const
{
    static const TGPicture * picture = gClient->GetPicture("folder__32x32");
    return picture;
}

//ClassImp(OHFolder)

OHFolder::OHFolder( const std::string & name, OHFolder * parent, time_t seconds, OHEntry::Type type )
  : OHEntry( name, seconds, type ),
    parent_( parent ),
    parse_( true ),
    server_( parent ? parent->GetServer() : "" ),
    provider_( parent ? parent->GetProvider() : "" ),
    partition_( parent ? parent->GetPartition() : "" )
{
    std::string::size_type pos = name.find_last_of( OHConfigDialog::Delimiters );
    std::string title = name;
    if ( std::string::npos != pos )
    {
        title = name.substr( pos + 1 );
    }
    SetTitle( title.c_str() );
}

OHFolder::OHFolder( const std::string & name, time_t time, bool parse )
  : OHEntry( name, time ),
    parent_( 0 ),
    parse_( parse )
{ ; }

OHFolder::~OHFolder( )
{
    RemoveChildren();
}

void 
OHFolder::RemoveChildren()
{
    folders_.clear();
}

const char * 
OHFolder::GetServer( ) const 
{
    return server_.c_str();
}

const char * 
OHFolder::GetProvider( ) const 
{
    return provider_.c_str();
}

const char * 
OHFolder::GetPartition( ) const 
{
    return partition_.c_str();
}

void
OHFolder::AddChildItem( const std::string & name, OHFolder * object, OHFolder * parent, int level )
{
    if ( !parse_ )
    {
	folders_.insert( std::make_pair( object->GetName(), std::shared_ptr<OHFolder>( object ) ) );
	return ;            
    }
    
    std::string::size_type beg = 0;
    std::string::size_type end = std::string::npos;
    for ( int i = 0; i < level; )
    {
        beg = end + 1;
    	end = name.find_first_of( OHConfigDialog::Delimiters, beg );
        if ( std::string::npos == end )
        {
            ERS_DEBUG( 1 , "FOLDER " << GetName() << ": Insert new object "
                    << object->GetName() << " " << object->GetTitle() );
            folders_.insert( std::make_pair( object->GetName(), std::shared_ptr<OHFolder>( object ) ) );
            return ;
        }
        if ( end != beg ) // empty folders are skept
        {
            ++i;
        }
    }
    std::string folder_long_name = name.substr( 0, end );
    std::string folder_short_name = name.substr( beg, end - beg );
    
    std::map<std::string,std::shared_ptr<OHFolder> >::iterator it = folders_.find( folder_short_name );
    
    std::shared_ptr<OHFolder> folder;
    if ( it == folders_.end() )
    {
        ERS_DEBUG( 1, "FOLDER " << GetName() << ": Insert new folder "
                << object->GetName() << " " << object->GetTitle() );
        folder.reset( new OHFolder( folder_long_name, parent ) );
        folders_.insert( std::make_pair( folder_short_name, folder ) );
    }
    else
    {
    	folder = it->second;
    }
    
    folder->AddChildItem( name, object, parent, level + 1 );
}

void
OHFolder::Browse( TBrowser * browser )
{    
    OHRootBrowser * browser_impl = (OHRootBrowser*)browser->GetBrowserImp();
    
    if ( browser_impl->IsRefresh() )
    {
    	RemoveChildren();
        if ( parent_ )
        {
            parent_->Refresh( browser, std::string( GetName() ) + ".*" );
        }
    }
    Explore( browser );
}

void
OHFolder::Explore( TBrowser * browser )
{
    for (   std::map<std::string,std::shared_ptr<OHFolder> >::iterator it = folders_.begin();
	    it != folders_.end(); ++it )
    {
        browser->Add( it->second.get() );
    }
}

void 
OHFolder::SaveToFile( TGMainFrame * frame, const std::string & ) const
{
    if ( parent_ )
    {
        parent_->SaveToFile( frame, std::string( GetName() ) + ".*" );
    }
}	

void
OHFolder::SendCommand( TGMainFrame * , const std::string & ) const
{
}
