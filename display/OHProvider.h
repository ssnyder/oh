/* 
   OHProvider.h

   Monika Barczyk & Piotr Golonka, May 2002
   Serguei Kolos, Jan 2004
*/

#ifndef _OH_PROVIDER_H_
#define _OH_PROVIDER_H_

#include <map>
#include <string>

#include "OHFolder.h"

#include <TBrowser.h>

class OHProvider: public OHFolder
{  
    std::string	server_;
    OHFolder objects_holder_;
      
    const TGPicture * GetSmallPicture() const;
    const TGPicture * GetLargePicture() const;
  
 public:
    OHProvider( const std::string & name,
		const std::string & server,
		const OWLTime & time,
		OHFolder * parent )
    :  OHFolder( name, parent, time.c_time() ),
       server_( server ),
       objects_holder_( "Holder" )
    { ; }
    	      
    Bool_t IsFolder() const { return kTRUE; }

    int GetTag() const { return 0; }
    const char * GetServer( ) const { return server_.c_str(); }
    const char * GetProvider( ) const { return GetName(); }
    Bool_t CommandsAllowed() const { return kTRUE; }

    void Refresh( TBrowser * browser, const std::string & mask );
    void Browse(TBrowser * browser);
    void SaveToFile( TGMainFrame * frame, const std::string & mask = "" ) const;
    void SendCommand( TGMainFrame * frame, const std::string & command ) const;   
  
    //    ClassDef(OHProvider,1)
};

#endif






