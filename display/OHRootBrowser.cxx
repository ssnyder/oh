/////////////////////////////////////////////
//
// This code is based on TRootBrowser.cxx
// (c) by the ROOT team.
//
// (c) Monika Barczyk and Piotr Golonka, 
//     May 2002
// (c) Serguei Kolos
//     May 2007
/////////////////////////////////////////////
#include <sstream>

#include "OHRootBrowser.h"
#include "OHDisplayHelp.h"
#include "OHProvider.h"
#include "OHPartition.h"
#include "OHObject.h"
#include "OHSystem.h"
#include "OHConfigDialog.h"
#include "OHContextMenu.h"

#include <TROOT.h>
#include <TClass.h>
#include <TGListView.h>
#include <TGSplitter.h>
#include <TG3DLine.h>
#include <TGMsgBox.h>
#include <TRootHelpDialog.h>
#include <TVirtualX.h>

const int OHRootBrowser::kShowHistory = 45678;

// Browser menu command ids
enum ERootBrowserCommands 
{   
    kFileQuit = 1001,
    kFileSave,
    
    kViewToolBar,
    kViewStatusBar,
    
    kCmdRefresh,
    kNewCommand,
        
    kViewArrangeByName,
    kViewArrangeByTime,
    kViewArrangeByTag,
    kViewArrangeByType,

    kViewLargeIcons,
    kViewSmallIcons,
    kViewList,
    kViewDetails,

    kViewStyleDefault,
    kViewStyleColor,
    
    kPreferencesSingleCanvas = OHRootBrowser::kSingleCanvas,
    kPreferencesMultiCanvas = OHRootBrowser::kNewCanvas,
    kPreferencesMultiPad = OHRootBrowser::kMultiPad,
    kPreferencesHistory = OHRootBrowser::kShowHistory,
    kPreferencesConfig,
        
    kHelpAbout,
    kHelpUsersGuide
};

// Toolbar description
ToolBarData_t gToolBarData[] = {
  { "exit__16x16",		"Exit",			kFALSE,	kFileQuit, 0 },
  { "",				"",			kTRUE,  0, 0 },
  { "save__16x16",		"Save",			kFALSE,	kFileSave, 0 },
  { "",				"",			kTRUE,  0, 0 },
  { "refresh__16x16",		"Refresh",		kFALSE, kCmdRefresh, 0 },
  { "",				"",			kTRUE,  0, 0 },
  { "send__16x16",		"Send Command",		kFALSE, kNewCommand, 0 },
  { "",				"",			kTRUE,  0, 0 },
  { "view_large_icons__16x16",	"Large Icons",		kTRUE,	kViewLargeIcons, 0 },
  { "view_small_icons__16x16",	"Small Icons",		kTRUE,  kViewSmallIcons, 0 },
  { "view_columns__16x16",	"List",			kTRUE,  kViewList, 0 },
  { "view_details__16x16",   	"Details",		kTRUE,  kViewDetails, 0 },
  { "",				"",			kTRUE,  0, 0 },
  { "history__16x16",		"Show History",		kTRUE,  kPreferencesHistory, 0 },
  { "",				"",			kTRUE,  0, 0 },
  { "single_canvas__16x16",	"Single Canvas",	kTRUE,  kPreferencesSingleCanvas, 0 },
  { "multi_canvas__16x16",	"Multi Canvas",		kTRUE,  kPreferencesMultiCanvas, 0 },
  { "multi_pad__16x16",		"Multi Pad",		kTRUE,  kPreferencesMultiPad, 0 },
  { "",				"",			kTRUE,  0, 0 },
  { "folder_wrench__16x16",	"Browser Configuration",kFALSE, kPreferencesConfig, 0 },
  { 0, 0, kFALSE, 0, 0 }
};

enum Buttons { kButtonSave = 2, kButtonRefresh = 4, kButtonSend = 6 };

struct WaitCursor
{
    WaitCursor( Int_t wid, Int_t wid1 = 0, Int_t wid2 = 0 )
      : wid_( wid ),
        wid1_( wid1 ),
        wid2_( wid2 )
    {
        static Cursor_t WaitCursor = gVirtualX->CreateCursor(kWatch);
        gVirtualX->SetCursor(wid_, WaitCursor);
        if ( wid1_ ) gVirtualX->SetCursor(wid1_, WaitCursor);
        if ( wid2_ ) gVirtualX->SetCursor(wid2_, WaitCursor);
	gVirtualX->Update();
    }
    
    ~WaitCursor()
    {
	gVirtualX->SetCursor(wid_, kNone);
        if ( wid1_ ) gVirtualX->SetCursor(wid1_, kNone);
        if ( wid2_ ) gVirtualX->SetCursor(wid2_, kNone);
    }
    
  private:
    Int_t wid_;
    Int_t wid1_;
    Int_t wid2_;
};

OHRootBrowser::OHRootBrowser(TBrowser *b, const char *name, UInt_t width, UInt_t height)
  : TGMainFrame(gClient->GetRoot(), width, height), TBrowserImp(b)
{
    CreateBrowser(name);
    Resize(width, height);
    Show();
}

OHRootBrowser::OHRootBrowser(TBrowser *b, const char *name, Int_t x, Int_t y, UInt_t width, UInt_t height)
  : TGMainFrame(gClient->GetRoot(), width, height), TBrowserImp(b)
{
    CreateBrowser(name);
    MoveResize(x, y, width, height);
    SetWMPosition(x, y);
    Show();
}

OHRootBrowser::~OHRootBrowser()
{
    // Browser destructor.
    delete fToolBar;
    delete fToolBarSep;
    delete fStatusBar;
    delete fIconBox;
    delete fPictures;

    delete fGarbage;
}

void OHRootBrowser::CreateBrowser(const char *name)
{        
    fGarbage = new TList();
    fGarbage->SetOwner( kTRUE );
    
    // Create menus
    fFileMenu = new TGPopupMenu(fClient->GetRoot());
    fFileMenu->AddEntry("&Save...", kFileSave);
    fFileMenu->AddEntry("&Quit", kFileQuit);
    fFileMenu->DisableEntry( kFileSave );
        
    fViewMenu = new TGPopupMenu(fClient->GetRoot());
    fGarbage->Add( fViewMenu );
    fViewMenu->AddEntry("&Toolbar", kViewToolBar);
    fViewMenu->AddEntry("&Status Bar", kViewStatusBar);
    fViewMenu->AddSeparator();
    
    fViewAsMenu = new TGPopupMenu(fClient->GetRoot());
    fViewAsMenu->AddEntry("Lar&ge Icons", kViewLargeIcons);
    fViewAsMenu->AddEntry("S&mall Icons", kViewSmallIcons);
    fViewAsMenu->AddEntry("&List", kViewList);
    fViewAsMenu->AddEntry("&Details", kViewDetails);
    
    fSortMenu = new TGPopupMenu(fClient->GetRoot());
    fSortMenu->AddEntry("By &Name", kViewArrangeByName);
    fSortMenu->AddEntry("By &Time", kViewArrangeByTime);
    fSortMenu->AddEntry("By &Tag", kViewArrangeByTag);
    fSortMenu->AddEntry("By T&ype", kViewArrangeByType);

    fViewMenu->AddPopup("&Arrange Items", fSortMenu);
    fViewMenu->AddPopup("&Display Items As", fViewAsMenu);
    
    fViewMenu->CheckEntry(kViewToolBar);
    fViewMenu->CheckEntry(kViewStatusBar);
    
    fStyleMenu = new TGPopupMenu (fClient->GetRoot());
    fGarbage->Add( fStyleMenu );
    fStyleMenu->AddEntry("&Default", kViewStyleDefault);
    fStyleMenu->AddEntry("&Color", kViewStyleColor );
    fViewMenu->AddPopup("Canvas &Style",fStyleMenu);

    fPreferencesMenu = new TGPopupMenu(fClient->GetRoot());
    fCanvasMenu = new TGPopupMenu(fClient->GetRoot());
    fCanvasMenu->AddEntry("&Single Canvas", kSingleCanvas);
    fCanvasMenu->AddEntry("New &Canvas", kNewCanvas);
    fCanvasMenu->AddEntry("New &Pad", kMultiPad);
    
    fPreferencesMenu->AddEntry("Show &History", kPreferencesHistory);
    fPreferencesMenu->AddPopup("Show Histograms In", fCanvasMenu);
    fPreferencesMenu->AddSeparator();
    fPreferencesMenu->AddEntry("&Browsing Configuration ...", kPreferencesConfig);

    TGPopupMenu * fHelpMenu = new TGPopupMenu(fClient->GetRoot());
    fGarbage->Add( fHelpMenu );
    fHelpMenu->AddEntry("&User's Guide",kHelpUsersGuide);
    fHelpMenu->AddEntry("&About Histogram Display", kHelpAbout);
    
    // This main frame will process the menu commands
    fFileMenu->Associate(this);
    fViewMenu->Associate(this);
    fStyleMenu->Associate(this);
    fPreferencesMenu->Associate(this);
    fHelpMenu->Associate(this);
    
    // Create menubar layout hints
    TGLayoutHints * menubar_item_layout = new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 4, 0, 0);
    TGLayoutHints * menubar_help_layout = new TGLayoutHints(kLHintsTop | kLHintsRight);
    fGarbage->Add( menubar_item_layout );
    fGarbage->Add( menubar_help_layout );
    
    // Create menubar
    TGMenuBar * fMenuBar = new TGMenuBar(this, 1, 1, kHorizontalFrame);
    fMenuBar->AddPopup("&File",        fFileMenu,        menubar_item_layout);
    fMenuBar->AddPopup("&View",        fViewMenu,        menubar_item_layout);
    fMenuBar->AddPopup("&Preferences", fPreferencesMenu, menubar_item_layout);
    fMenuBar->AddPopup("&Help",        fHelpMenu,        menubar_help_layout);
    
    TGLayoutHints * menubar_layout = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 0, 0, 1, 1);
    fGarbage->Add( menubar_layout );
    AddFrame(fMenuBar, menubar_layout);
    
    // Create toolbar and separator
    fToolBarSep = new TGHorizontal3DLine(this);
    fToolBar = new TGToolBar(this, 60, 20, kHorizontalFrame);
    
    fPictures = new OHPictures( fToolBar->GetClient() );

    int spacing = 8;
    for (int i = 0; gToolBarData[i].fPixmap; i++) 
    {
        if (strlen(gToolBarData[i].fPixmap) == 0)
        {
            spacing = 8;
            continue;
        }
        fToolBar->AddButton(this, &gToolBarData[i], spacing);
	    spacing = 0;
    }    
        
    fCmdHistoryMenu = new OHContextMenu( fClient->GetRoot() );
    fCmdHistoryMenu->AddEntry("New ...", kNewCommand);
    fCmdHistoryMenu->AddSeparator( );
    
    fCommandMenu = new OHContextMenu( fClient->GetRoot(), fCmdHistoryMenu );
    fCommandMenu->AddEntry("Save...", kFileSave);
    fCommandMenu->AddPopup("Send Command", fCmdHistoryMenu);
    fCommandMenu->Associate(this);
    
    fDefaultMenu  = new OHContextMenu( fClient->GetRoot() );
    fDefaultMenu->AddEntry("Save...", kFileSave);
    fDefaultMenu->Associate(this);
    
    TGLayoutHints * toolbar_layout = new TGLayoutHints(kLHintsTop | kLHintsExpandX);
    fGarbage->Add( toolbar_layout );
    AddFrame(fToolBarSep, toolbar_layout);
    AddFrame(fToolBar, toolbar_layout);
    
    // Create panes
    TGHorizontalFrame * fHf = new TGHorizontalFrame(this, 10, 10);
    fGarbage->Add( fHf );
    
    TGVerticalFrame * fV1 = new TGVerticalFrame(fHf, 10, 10, kFixedWidth);
    TGVerticalFrame * fV2 = new TGVerticalFrame(fHf, 10, 10);
    fGarbage->Add( fV1 );
    fGarbage->Add( fV2 );
    TGCompositeFrame * fTreeHdr = new TGCompositeFrame(fV1, 10, 10, kSunkenFrame);
    fGarbage->Add( fTreeHdr );
    
    TGLabel * fLabel = new TGLabel(fTreeHdr, "Online Histogramming Service");
    fGarbage->Add( fLabel );
    
    TGLayoutHints * layout1 = new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 3, 0, 0, 0);    
    fGarbage->Add( layout1 );
    fTreeHdr->AddFrame(fLabel, layout1);
    
    TGLayoutHints * layout2 = new TGLayoutHints(kLHintsTop | kLHintsExpandX, 0, 0, 1, 2);
    fGarbage->Add( layout2 );
    fV1->AddFrame(fTreeHdr, layout2);
    fV1->Resize(fTreeHdr->GetDefaultWidth() + 200, fV1->GetDefaultHeight());
    
    TGLayoutHints * layout3 = new TGLayoutHints(kLHintsLeft | kLHintsExpandY);
    fGarbage->Add( layout3 );
    fHf->AddFrame(fV1, layout3);
    
    TGVSplitter * fSplitter = new TGVSplitter(fHf);
    fGarbage->Add( fSplitter );
    fSplitter->SetFrame(fV1, kTRUE);
    fHf->AddFrame(fSplitter, layout3);
    
    TGLayoutHints * layout4 = new TGLayoutHints(kLHintsRight | kLHintsExpandX | kLHintsExpandY);
    fGarbage->Add( layout4 );
    fHf->AddFrame(fV2, layout4);
    
    // Create tree
    TGCanvas * fTreeView = new TGCanvas(fV1, 10, 10, kSunkenFrame | kDoubleBorder);
    fGarbage->Add( fTreeView );
    
    fListTree = new TGListTree(fTreeView->GetViewPort(), 10, 10, kHorizontalFrame, fgWhitePixel);
    fListTree->Associate(this);
    fListTree->SetAutoTips();
    fTreeView->SetContainer(fListTree);
    fListTree->SetCanvas(fTreeView);
    
    TGLayoutHints * layout5 = new TGLayoutHints(kLHintsExpandX | kLHintsExpandY);
    fGarbage->Add( layout5 );
    fV1->AddFrame(fTreeView, layout5);
    
    // Create list view (icon box)
    fIconBox = new OHLVContainer(fV2, 800, 600, this );
    fIconBox->Associate(this);
    
    AddFrame(fHf, layout5);

    // Statusbar
    int parts[] = { 25, 75 };
    fStatusBar = new TGStatusBar(this, 60, 10);
    fStatusBar->SetParts(parts, 2);
    TGLayoutHints * layout6 = new TGLayoutHints(kLHintsBottom | kLHintsExpandX, 0, 0, 3, 0);
    fGarbage->Add( layout6 );
    AddFrame(fStatusBar, layout6);
    
    // Misc
    SetWindowName(name);
    SetIconName(name);
    SetIconPixmap("rootdb_s.xpm");
    SetClassHints("Browser", "Browser");
    
    SetMWMHints(kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
        
    MapSubwindows();

    fStyleDefault = new TStyle ("B&W", "Default");
    fStyleDefault->SetOptStat("ourmen");
    fStyleDefault->SetTitleFontSize(0.033);
    fStyleDefault->SetStatFontSize(0.033);
    fStyleDefault->SetTitleSize(0.03,"xyz");
    fStyleDefault->SetLabelSize(0.03,"xyz");
    fStyleDefault->SetLabelFont(42,"xyz");
    fStyleDefault->SetTitleXOffset(1.2);
    fStyleDefault->SetTitleYOffset(1.6);
    
    fStyleColor = new TStyle (*fStyleDefault);
    fStyleColor->SetHistFillStyle(1001);
    fStyleColor->SetHistFillColor(42);
    fStyleColor->SetHistLineColor(631);
    fStyleColor->SetLabelColor(631,"xyz");
    fStyleColor->SetAxisColor(631,"xyz");
    fStyleColor->SetTitleColor(436,"xyz");
    fStyleColor->SetTitleTextColor(436);
    fStyleColor->SetStatTextColor(436);
        
    gROOT->ForceStyle();

    // we need to use GetDefaultSize() to initialize the layout algorithm...
    Resize(GetDefaultSize());
    
    fRefresh = kFALSE;
    fAscending = kTRUE;
    SetViewMode( kLVDetails );
    SetSortMode( OHLVEntry::kSortByName );
    SetStyleMode( kViewStyleDefault );
    SetCanvasMode( kSingleCanvas );
    
    fSelectedTreeItem = 0;
    fSelectedEntry = 0;
    fViewMode = kLVDetails;
    fCanvasMode = kSingleCanvas;

    SetToolStates();
}

void OHRootBrowser::Add( TObject * obj, const char *, int )
{
    // Add items to the browser. This function has to be called
    // by the Browse() member function of objects when they are
    // called by a browser.
    OHEntry * entry = dynamic_cast<OHEntry *>( obj );
    if ( entry )
    {	  
        if ( fSelectedTreeItem )
        {
            fIconBox->AddItem( new OHLVEntry( fIconBox, entry, fViewMode ) );
	}
         
	if ( entry->IsFolder() )
        {
            const TGPicture * pic = entry->GetSmallPicture();
	    fListTree->AddItem( fSelectedTreeItem, obj->GetTitle(), obj, pic, pic );
        }
    }
}

void OHRootBrowser::BrowseObj(TObject *obj)
{
    // Browse object. This, in turn, will trigger the calling of
    // OHRootBrowser::Add() which will fill the IconBox and the tree.
    if ( dynamic_cast<OHEntry *>( obj ) )
    {
	WaitCursor wc( fId, fIconBox->GetId(), fListTree->GetId() );
	fIconBox->RemoveAll();
	obj->Browse(fBrowser);
	fIconBox->SetViewMode( fViewMode );
	fIconBox->Refresh();
    }
}

void OHRootBrowser::CloseWindow()
{
    // In case window is closed via WM we get here.
    _exit( 0 );
}

void OHRootBrowser::DisplayTotal(Int_t total, Int_t selected)
{
    char tmp[64];
  
    sprintf(tmp, "%d Object%s, %d selected.", total, (total == 1) ? "" : "s", selected);
    fStatusBar->SetText(tmp, 0);
}

void OHRootBrowser::ClearCurrentSelection( TGListTreeItem * item )
{
    if ( fSelectedTreeItem && item ) 
    {
        TGListTreeItem * selected_parent;
        for ( TGListTreeItem * sit = fSelectedTreeItem; (selected_parent = sit -> GetParent()); sit = selected_parent )
        {
	    TGListTreeItem * parent;
	    for ( TGListTreeItem * it = item; (parent = it -> GetParent()); it = parent )
	    {
		if ( it == sit )
                {
		    return;
                }
                
                if ( parent == selected_parent )
		{
		    fListTree->DeleteChildren( sit );
		    return ;
		}
	    }
	}
    }
}
    
void OHRootBrowser::ListTreeHighlight( TGListTreeItem * item )
{
    ClearCurrentSelection( item );
    
    fSelectedTreeItem = item;

    if ( !fSelectedTreeItem ) 
    {
    	return;
    }
         
    WaitCursor wc( fId, fIconBox->GetId(), fListTree->GetId() );

    fListTree->DeleteChildren( fSelectedTreeItem );
    if (fSelectedTreeItem->IsActive())
    {
	BrowseObj((TObject*)fSelectedTreeItem->GetUserData());
    }
    fListTree->OpenItem( fSelectedTreeItem );
    
    fListTree->ClearViewPort();
    
    fListTree->SortChildren( fSelectedTreeItem );
}

void OHRootBrowser::ListViewAction(OHEntry *obj)
{
  // Default action when double clicking on icon.
    if (obj && fSelectedTreeItem) 
    {
	if ( obj->IsFolder() )
        {
	    TGListTreeItem * child = fListTree->FindItemByObj( fSelectedTreeItem, obj );
	    if ( child )
	    {
		fListTree->UnselectAll( kFALSE );
		fListTree->OpenItem( fSelectedTreeItem );
		fListTree->SetSelected( child );
		fListTree->HighlightItem( child, kTRUE, kFALSE );
		fListTree->HighlightItem( fSelectedTreeItem, kFALSE, kFALSE );
		SendMessage(this, MK_MSG(kC_LISTTREE, kCT_ITEMCLICK), kButton1, 0);
	    }
        }
        else
        {
            obj->Browse(fBrowser);
        }
    }
}

void OHRootBrowser::ShowHideToolBar()
{
    if (fViewMenu->IsEntryChecked(kViewToolBar)) 
    {
	HideFrame(fToolBar);
	HideFrame(fToolBarSep);
	fViewMenu->UnCheckEntry(kViewToolBar);
    } 
    else 
    {
	ShowFrame(fToolBar);
	ShowFrame(fToolBarSep);
	fViewMenu->CheckEntry(kViewToolBar);
    }
}

void OHRootBrowser::ShowHideStatusBar()
{
    if (fViewMenu->IsEntryChecked(kViewStatusBar))
    {
	HideFrame(fStatusBar);
	fViewMenu->UnCheckEntry(kViewStatusBar);
    } 
    else
    {
	ShowFrame(fStatusBar);
	fViewMenu->CheckEntry(kViewStatusBar);
    }
}

void OHRootBrowser::SetViewMode(EListViewMode new_mode)
{
    if ( fViewMode != new_mode ) 
    {
	fViewMode = new_mode;
	fViewAsMenu->RCheckEntry((int)kViewLargeIcons + (int)fViewMode, kViewLargeIcons, kViewDetails);
      
	for ( int i = kLVLargeIcons; i <= kLVDetails; ++i )
        {
	    gToolBarData[i-kLVLargeIcons + 8].fButton->SetState((i == fViewMode) ? kButtonEngaged : kButtonUp );
        }
	fIconBox->SetViewMode(fViewMode);
    }
    fIconBox->Refresh();
}

void OHRootBrowser::SetSortMode(OHLVEntry::SortMode mode)
{
    fSortMode = mode;
    if ( fSortMenu->IsEntryRChecked( (int)kViewArrangeByName + (int)mode ) )
    {
    	fAscending = !fAscending;
    }
    fSortMenu->RCheckEntry( (int)kViewArrangeByName + (int)mode, kViewArrangeByName, kViewArrangeByType );
  
    fIconBox->Sort( mode, fAscending );
}

void OHRootBrowser::SetCanvasMode(ECanvasMode new_mode)
{
    if ( fCanvasMode != new_mode ) 
    {
	fCanvasMode = new_mode;
	fCanvasMenu->RCheckEntry(fCanvasMode, kSingleCanvas, kMultiPad );
      
	for ( int i = kSingleCanvas; i <= kMultiPad; ++i )
        {
	    gToolBarData[i-kSingleCanvas + 15].fButton->SetState((i == fCanvasMode) ? kButtonEngaged : kButtonUp );
        }
    }
}

void OHRootBrowser::SetStyleMode(Int_t new_mode) 
{
    switch (new_mode) 
    {
	case  kViewStyleColor:
	    fStyleColor->cd();
	    break;
        case kViewStyleDefault:
	default:
            fStyleDefault->cd();
            break;

    }
    fStyleMenu->RCheckEntry(new_mode, kViewStyleDefault, kViewStyleColor);
}

void OHRootBrowser::SetToolStates( )
{
    if ( fSelectedEntry )
    {
	fFileMenu->EnableEntry( kFileSave );
	gToolBarData[kButtonSave].fButton->SetState( kButtonUp );
	gToolBarData[kButtonRefresh].fButton->SetState( kButtonUp );
        gToolBarData[kButtonSend].fButton->SetState( fSelectedEntry->CommandsAllowed() ? kButtonUp : kButtonDisabled );
    }
    else
    {
	fFileMenu->DisableEntry( kFileSave );
	gToolBarData[kButtonSave].fButton->SetState( kButtonDisabled );
	gToolBarData[kButtonRefresh].fButton->SetState( kButtonDisabled );
	gToolBarData[kButtonSend].fButton->SetState( kButtonDisabled );
    }
}

void OHRootBrowser::SetPreferences( Int_t item ) 
{
    if ( fPreferencesMenu->IsEntryChecked(item) )
    {
	fPreferencesMenu->UnCheckEntry(item);
    } 
    else
    {
	fPreferencesMenu->CheckEntry(item);
    }
    if ( item == kShowHistory )
    {
    	RefreshListView();
    }
}

Bool_t OHRootBrowser::GetPreferences( int item )
{
    return fPreferencesMenu->IsEntryChecked(item);
}

void OHRootBrowser::RefreshListView( )
{
    if ( !fSelectedTreeItem )
	return;

    fRefresh = kTRUE;
    fIconBox->RemoveAll();
    ListTreeHighlight( fSelectedTreeItem );
    fRefresh = kFALSE;
}

Bool_t OHRootBrowser::ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2)
{
    // Handle menu and other command generated by the user.
    TRootHelpDialog * hd;
    switch (GET_MSG(msg)) 
    {
    case kC_COMMAND:
      
      switch (GET_SUBMSG(msg)) 
	{
	case kCM_BUTTON:
	case kCM_MENU:
	  
	  switch (parm1) 
	    {
	    case kFileQuit:
		_exit(0);
		break;
	    case kFileSave:
		if ( fSelectedEntry )
		{
		    WaitCursor wc( fId, fIconBox->GetId(), fListTree->GetId() );
		    fSelectedEntry->SaveToFile( this );
		}
	    break;	
					
	      // Handle View menu items...
	    case kViewToolBar:
		ShowHideToolBar();
		break;
	    case kViewStatusBar:
		ShowHideStatusBar();
		break;
	    case kViewLargeIcons:
	    case kViewSmallIcons:
	    case kViewList:
	    case kViewDetails:
		SetViewMode((EListViewMode)( parm1 - kViewLargeIcons ) );
		break;
	    case kCmdRefresh:
		RefreshListView();
		break;
	    case kNewCommand:
	        if ( fSelectedEntry )
                {
                    fSelectedEntry->SendCommand( this, "" );
                }
	        break;	      
	    case kViewArrangeByName:
	    case kViewArrangeByTime:
	    case kViewArrangeByTag:
            case kViewArrangeByType:
	      SetSortMode((OHLVEntry::SortMode)(parm1-kViewArrangeByName));
	      break;
	      // Handle Options menu items...

	    case kViewStyleDefault: // display styles
	    case kViewStyleColor:
		SetStyleMode((Int_t)parm1);
		break;

	    case kPreferencesHistory:
		SetPreferences((Int_t)parm1);
		break;

	    case kPreferencesConfig:
		{
		  OHConfigDialog * cd = new OHConfigDialog( this );
		  cd->Popup();
		}
		break;

	    case kSingleCanvas:
	    case kNewCanvas:
	    case kMultiPad:
		SetCanvasMode((ECanvasMode)parm1 );
		break;

	      // Handle Help menu items...
	    case kHelpUsersGuide:
	      {
		hd = new TRootHelpDialog(this, "OH Display User's Guide", 550, 300);
		hd->SetText(gOHHelpUsersGuide);
		hd->Popup();
	      }
	      break;
	    case kHelpAbout:
	      {
		hd = new TRootHelpDialog(this, "OH Display Version 3.1", 550, 300);
		hd->SetText( gOHHelpAbout );
		hd->Popup();
	      }
	      break;
	    }
	  break;
	  
	default:
	  break;
	}
      break;
      
    case kC_LISTTREE:
	if ( GET_SUBMSG(msg) == kCT_ITEMCLICK )
        {
            TGListTreeItem *item = fListTree->GetSelected();
	    if ( fSelectedTreeItem != item )
            { 
		if (GetPreferences(OHRootBrowser::kShowHistory))
                {
                    fSelectedTreeItem = item;
                    RefreshListView();
                }
		else
                {
                    ListTreeHighlight(item);
                }
	    }
            
            fSelectedEntry = (OHEntry *) fSelectedTreeItem->GetUserData();
 
            SetToolStates();
            
            if ( parm1 != kButton3 )
		break;

	    if ( fSelectedEntry && fSelectedEntry->CommandsAllowed() )
	    {
                Int_t x = (Int_t)(parm2 & 0xffff);
		Int_t y = (Int_t)((parm2 >> 16) & 0xffff);
		std::string command = fCommandMenu->Show( x, y );
		if ( !command.empty() )
		{
                    fSelectedEntry->SendCommand( this, command );
		}
	    }
            else
            {
		Int_t x = (Int_t)(parm2 & 0xffff);
		Int_t y = (Int_t)((parm2 >> 16) & 0xffff);
		fDefaultMenu->Show( x, y );
            }
        }
	else if ( GET_SUBMSG(msg) == kCT_ITEMDBLCLICK )
        {     
	    RefreshListView();
        }

	break;
      
    case kC_CONTAINER:
	if ( GET_SUBMSG(msg) == kCT_ITEMCLICK )
	{		
	    if ( parm1 != kButton3 ) 
		break;
                
	    Int_t x = (Int_t)(parm2 & 0xffff);
	    Int_t y = (Int_t)((parm2 >> 16) & 0xffff);
	    std::string command = fCommandMenu->Show( x, y );
	    if ( !command.empty() && fSelectedEntry )
	    {
		fSelectedEntry->SendCommand( this, command );
	    }
	}
	else if ( GET_SUBMSG(msg) == kCT_ITEMDBLCLICK )
	{
	    if ( parm1 == kButton1 && fIconBox->NumSelected() == 1) 
	    {
		void * current = 0;
		OHLVEntry * entry;
		if ((entry = (OHLVEntry *) fIconBox->GetNextSelected(&current)) != 0)
                {
		    ListViewAction(entry->entry());
                }
	    }
	}
	else if ( GET_SUBMSG(msg) == kCT_SELCHANGED )
	{
	    DisplayTotal((Int_t)parm1, (Int_t)parm2);
	    void * current = 0;
	    OHLVEntry * list_entry = (OHLVEntry*)fIconBox->GetNextSelected(&current);
	    if ( list_entry )
	    {
		fSelectedEntry = list_entry->entry();            
	    }
	    else if ( fSelectedTreeItem )
	    {
		fSelectedEntry = (OHEntry*)fSelectedTreeItem->GetUserData();
	    }
            
            SetToolStates();
	}
	break;
      
    default:
	break;
    }
    return kTRUE;
}

