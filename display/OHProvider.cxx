/* 
   OHProvider.cxx
   
   Monika Barczyk & Piotr Golonka, May 2002
   Serguei Kolos, Jan 2004
*/
#include <map>

#include <TGMsgBox.h>

#include "OHProvider.h"
#include "OHObject.h"
#include "OHPartition.h"
#include "OHRootBrowser.h"
#include "OHConfigDialog.h"

#include <oh/OHFileSaver.h>
#include <oh/OHIterator.h>
#include <oh/OHCommandSender.h>

//ClassImp(OHProvider)

const TGPicture * 
OHProvider::GetSmallPicture() const
{
    static const TGPicture * picture = gClient->GetPicture("provider__16x16");
    return picture;
}
  
const TGPicture * 
OHProvider::GetLargePicture() const
{
    static const TGPicture * picture = gClient->GetPicture("provider__32x32");
    return picture;
}

void
OHProvider::Browse( TBrowser * browser )
{
    objects_holder_.RemoveChildren();
    Refresh( browser, OHConfigDialog::ObjectRegex );
    objects_holder_.Explore( browser );
    OHFolder::Explore( browser );
}

void
OHProvider::Refresh( TBrowser * browser, const std::string & mask )
{
    OHRootBrowser * browser_impl = (OHRootBrowser*)browser->GetBrowserImp();
    
    IPCPartition partition( GetPartition() );
    
    try
    {
	OHIterator it( partition, server_, GetName(), mask );

	while ( it++ )
	{
	    OHEntry::Type type = it.isHistogram() ? OHObject::Histogram
	            : it.isGraph() ? OHObject::Graph
	            : it.isGraph() ? OHObject::Graph2D : OHObject::Efficiency;
	    if ( browser_impl->GetPreferences(OHRootBrowser::kShowHistory) )
	    {
            	std::vector< std::pair<int,OWLTime> > tags;
                it.tags( tags );
                for ( size_t i = 0; i < tags.size(); ++i )
                {
                    objects_holder_.AddChildItem( it.name(),
                            new OHObject( it.name(), tags[i].second, type, tags[i].first, this ), this );
                }
	    }
            else
            {
                objects_holder_.AddChildItem( it.name(),
                        new OHObject( it.name(), it.time(), type, it.tag(), this ), this );
            }
	}
    }
    catch ( ers::Issue & ex )
    {
	ERS_DEBUG( 0, ex );
    }
}

void 
OHProvider::SaveToFile( TGMainFrame * frame, const std::string & mask ) const
{
    std::string filename = GetPartition();
    filename += "-";
    filename += GetName();
    filename = AskFileName( frame, filename );
    
    if ( filename.empty() )
    {
    	return;
    }
    
    OHFileSaver receiver( filename );
  
    if ( receiver.error() )
    {
	std::ostringstream out;
	out << "Can not create '" << filename << "' file";
	std::string message = out.str();
	new TGMsgBox( gClient->GetRoot(), frame, "Error", message.c_str(), kMBIconExclamation );
	return ;
    }

    IPCPartition pp( GetPartition() );	
    try
    {
	OHIterator it( pp, server_, mask.empty() ? std::string( GetName() ) : mask,
	        OHConfigDialog::ObjectRegex );

	while ( it++ )
	{
	    it.retrieve( receiver, true );
	}
    }
    catch ( ers::Issue & ex )
    {
	ERS_DEBUG( 0, ex );
    }
}	

void
OHProvider::SendCommand( TGMainFrame * frame, const std::string & cmd ) const
{
    std::string command = cmd;
    if ( command.empty() )
    {
	std::ostringstream out;
	out << "Send command to '" << GetName() << "' provider";
    	command = AskCommand( frame, out.str() );
    }
    
    if ( command.empty() )
    {
    	return;
    }

    IPCPartition partition( GetPartition() );
    OHCommandSender cs( partition );

    try {
	cs.sendCommand( GetServer(), GetName(), command );
    }
    catch( ers::Issue & ex ) {
	new TGMsgBox( gClient->GetRoot(), frame, "Send Command Failed",
			"Histogram provider doesn't exist",
			kMBIconExclamation );
    }
}
