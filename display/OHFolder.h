/* 
   OHFolder.h

   Monika Barczyk & Piotr Golonka, May 2002
   Serguei Kolos, Jan 2004
*/

#ifndef _OH_FOLDER_H_
#define _OH_FOLDER_H_

#include <map>
#include <memory>
#include <string>

#include <owl/time.h>

#include "OHEntry.h"

#include <TBrowser.h>

class OHFolder : public OHEntry
{  
#ifndef __MAKECINT__
    std::multimap<std::string,std::shared_ptr<OHFolder> > folders_;
#endif

    OHFolder *	parent_;
    bool 	parse_;
    std::string server_;
    std::string provider_;
    std::string partition_;

    const TGPicture * GetSmallPicture() const;
    const TGPicture * GetLargePicture() const;
      
 public:
    OHFolder(	const std::string & name,
                OHFolder * parent,
                time_t seconds = 0,
                OHEntry::Type type = OHEntry::Folder );
                
    OHFolder(	const std::string & name, time_t time = 0, bool parse = true );
                
    virtual ~OHFolder( );
	      
    void AddChildItem( const std::string & name, OHFolder * object, OHFolder * parent, int level = 1 );
    void RemoveChildren();
    
    Bool_t IsFolder() const { return kTRUE; }
    int GetTag() const { return 0; }
    const char * GetServer( ) const;
    const char * GetProvider( ) const;
    const char * GetPartition( ) const;
    void Browse(TBrowser * b);
    void Explore(TBrowser * b);
    void SaveToFile( TGMainFrame * frame, const std::string & mask = "" ) const;
    void SendCommand( TGMainFrame * frame, const std::string & command ) const;   
  
    virtual void Refresh( TBrowser * , const std::string & ) { ; }
    
    //    ClassDef(OHFolder,1)
};

#endif
