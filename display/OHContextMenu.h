#ifndef MY_CONTEXT_MENU_H
#define MY_CONTEXT_MENU_H

#include <TGMenu.h>

class OHContextMenu : public TGPopupMenu
{
    OHContextMenu * commands_;
    TGTransientFrame * synch_;
    const char * command_;
    
  public:
    OHContextMenu( const TGWindow * parent, OHContextMenu * commands = 0 );
    
    void AddElement( const char * item );
    const char * GetElement( int index );
    const char * GetLastElement( );
    const char * Show( int x, int y );
    const char * Show( const TGWindow * w, int x, int y );
    void Activated(Int_t id);
    void PoppedDown();
};

#endif
