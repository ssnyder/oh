/*
 histo_display.cxx

 An application designed for browsing of histograms
 and display of selected histogram
 published in the OH server of given partition,
 using the ROOT framework.

 Monika Barczyk May  2002
 Sergei Kolos   Sept 2004
 */

#include <ipc/core.h>

#include <OHDisplayGuiFactory.h>
#include <OHRootBrowser.h>
#include <TApplication.h>
#include <TThread.h>
#include <OHSystem.h>

/**********************************************************************************************/

int main(int argc, char ** argv)
{
    TThread::Initialize();

    IPCCore::init(argc, argv);

    TApplication app("Histogram Display", 0, 0);

    OHDisplayGuiFactory factory;
    TBrowser browser("Histogram Display", "Histogram Display", 50, 200, 1600, 1000);
    OHSystem system;

    browser.Add(&system);

    app.Run();
}
