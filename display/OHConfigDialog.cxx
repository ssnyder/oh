#include <fstream>

#include <oh/OHCommandSender.h>

#include "OHRootBrowser.h"
#include "OHConfigDialog.h"

#include <TGLabel.h>
#include <TGNumberEntry.h>
#include <TGButton.h>
#include <TGMsgBox.h>
#include <TVirtualX.h>

std::string OHConfigDialog::ConfigurationFileName = CreateFileName();
std::string OHConfigDialog::ServerRegex( ".*" );
std::string OHConfigDialog::ProviderRegex( ".*" );
std::string OHConfigDialog::ObjectRegex( ".*" );
std::string OHConfigDialog::Delimiters( "/" );

bool OHConfigDialog::initialised_ = OHConfigDialog::ReadFromFile();

OHConfigDialog::OHConfigDialog( TGMainFrame * frame )
  : TGTransientFrame( gClient->GetRoot(), frame, 500, 100 )
{
    ReadFromFile();

    fGarbage = new TList;
    fGarbage->SetOwner( kTRUE );

    TGVerticalFrame * Main = new TGVerticalFrame( this, 500, 100 );
    TGLayoutHints * El = new TGLayoutHints( kLHintsTop | kLHintsLeft | kLHintsExpandX, 5, 5, 2, 2 );
    
    AddFrame( Main, El );
    
    fGarbage->Add( Main ); 
    fGarbage->Add( El );

    SetWindowName( "OH Display Configuration" );
    SetIconName( "OH Display Configuration" );
    
    fServers = new TGTextEntry( Main, ServerRegex.c_str() );
    fProviders = new TGTextEntry( Main, ProviderRegex.c_str() );
    fHistograms = new TGTextEntry( Main, ObjectRegex.c_str() );
    fDelimiters = new TGTextEntry( Main, Delimiters.c_str() );

    TGLabel * label = new TGLabel( Main, "Regular expression for the IS servers which will be scanned for histograms" );
    Main->AddFrame( label, El );
    fGarbage->Add( label ); 
    Main->AddFrame( fServers, El );
    fGarbage->Add( fServers ); 

    label = new TGLabel( Main, "Regular expression for histogram providers to be browsed" );
    Main->AddFrame( label, El );
    fGarbage->Add( label ); 
    Main->AddFrame( fProviders, El );
    fGarbage->Add( fProviders ); 

    label = new TGLabel( Main, "Regular expression for histogram names to be browsed" );
    Main->AddFrame( label, El );
    fGarbage->Add( label ); 
    Main->AddFrame( fHistograms, El );
    fGarbage->Add( fHistograms ); 

    label = new TGLabel( Main, "These characters will be used for splitting histogram and provider names into folders" );
    Main->AddFrame( label, El );
    fGarbage->Add( label ); 
    Main->AddFrame( fDelimiters, El );
    fGarbage->Add( fDelimiters ); 
}


OHConfigDialog::~OHConfigDialog()
{
    WriteToFile();
    delete fGarbage;
}


std::string
OHConfigDialog::CreateFileName()
{
    std::string filename;
    const char * dir = getenv( "HOME" );
    if ( dir )
    {
    	filename = dir;
    }
    filename += "/.oh_display";
    return filename;
}

bool
OHConfigDialog::ReadFromFile()
{
    std::ifstream in( ConfigurationFileName.c_str() );
    if ( !in )
    {
    	return false;
    }
    std::getline( in, ServerRegex );
    std::getline( in, ProviderRegex );
    std::getline( in, ObjectRegex );
    std::getline( in, Delimiters );
    return static_cast<bool>(in);
}

void
OHConfigDialog::WriteToFile()
{
    std::ofstream out( ConfigurationFileName.c_str() );
    if ( !out )
    {
    	return ;
    }
    out << ServerRegex << std::endl;
    out << ProviderRegex << std::endl;
    out << ObjectRegex << std::endl;
    out << Delimiters << std::endl;
}

//______________________________________________________________________________
void OHConfigDialog::Popup()
{
    // Popup dialog.

    //--- create the OK, Apply and Cancel buttons

    UInt_t  nb = 0, width = 0, height = 0;

    TGHorizontalFrame * hf = new TGHorizontalFrame(this, 500, 20, kFixedWidth);
    TGLayoutHints     * l1 = new TGLayoutHints(kLHintsCenterY | kLHintsExpandX, 5, 5, 0, 0);

    // put hf as last in the list to be deleted
    fGarbage->Add(l1);

    fOk = new TGTextButton(hf, "&Ok", 11);
    fOk->Associate(this);
    hf->AddFrame(fOk, l1);
    height = fOk->GetDefaultHeight();
    width  = TMath::Max(width, fOk->GetDefaultWidth()); ++nb;
    
    fCancel = new TGTextButton(hf, "&Cancel", 12);
    fGarbage->Add(fCancel);
    fCancel->Associate(this);
    hf->AddFrame(fCancel, l1);
    height = fCancel->GetDefaultHeight();
    width  = TMath::Max(width, fCancel->GetDefaultWidth()); ++nb;

    // place buttons at the bottom
    l1 = new TGLayoutHints(kLHintsBottom | kLHintsCenterX, 0, 0, 5, 5);
    fGarbage->Add(l1);
    fGarbage->Add(hf);

    AddFrame(hf, l1);

    // keep the buttons centered and with the same width
    hf->Resize((width + 20) * nb, height);

    // map all widgets and calculate size of dialog
    MapSubwindows();

    width  = GetDefaultWidth();
    height = GetDefaultHeight();

    Resize(width, height);

    // position relative to the parent's window
    Window_t wdum;
    int ax, ay;
    gVirtualX->TranslateCoordinates(fMain->GetId(), GetParent()->GetId(),
                              (Int_t)(((TGFrame *) fMain)->GetWidth() - fWidth) >> 1,
                              (Int_t)(((TGFrame *) fMain)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
    Move(ax, ay);
    SetWMPosition(ax, ay);

    // make the message box non-resizable
    SetWMSize(width, height);
    SetWMSizeHints(width, height, width, height, 1, 1);

    SetMWMHints(kMWMDecorAll | kMWMDecorMaximize | kMWMDecorMinimize | kMWMDecorResizeH | kMWMDecorMenu,
		kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize | kMWMFuncMinimize | kMWMFuncClose,
		kMWMInputModeless );

    MapWindow();
    fClient->WaitFor(this);
}

Bool_t OHConfigDialog::ProcessMessage( Long_t msg, Long_t parm1, Long_t )
{
     if ( GET_MSG(msg) == kC_COMMAND )
     {	  
	switch (parm1) 
	{
	     case 11: // OK
	     {
		ServerRegex = fServers->GetText();
		ProviderRegex = fProviders->GetText();
		ObjectRegex = fHistograms->GetText();
		Delimiters = fDelimiters->GetText();
	     }
	     case 12: // Cancel
		delete this;
	     default:
		break;
	}
     }
     return kTRUE;
}
