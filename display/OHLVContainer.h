#ifndef _OH_LV_CONTAINER_H_
#define _OH_LV_CONTAINER_H_

#include <typeinfo>
#include <TGListView.h>

#include "OHEntry.h"

class OHRootBrowser;

class OHLVContainer : public TGLVContainer 
{
    struct Element : public TGFrameElement
    {
       OHLVContainer * fContainer;

       Bool_t IsSortable() const 
       { return kTRUE; }
       
       Int_t  Compare( const TObject * obj ) const 
       { return ((OHLVEntry*)fFrame)->CompareEntries( (OHLVEntry*)(((Element*)obj)->fFrame), fContainer->fSortMode ); }
    };
    
    friend struct Element;
    
  public:
    OHLVContainer( TGCompositeFrame * parent, UInt_t w, UInt_t h, OHRootBrowser * browser );
    void Refresh();
    void Sort( OHLVEntry::SortMode mode, Bool_t ascending );
    void AddFrame(TGFrame *f, TGLayoutHints *l);
    void AddItem(TGLVEntry* item);
    
  private:
    void Update();

    OHLVEntry::SortMode fSortMode;
    const std::type_info * fElementsClass;
};

#endif
