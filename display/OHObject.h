/*
  OHObject.h
  provides support for all the types of OH objects: OHObject, OHGraph, OHGraph2D

  Monika Barczyk & Piotr Golonka, May 2002
  Herbert Kaiser, Aug 2006
  Serguei Kolos, April 2006
*/

#ifndef _OH_OBJECT_H_
#define _OH_OBJECT_H_

#include <string>
#include <memory>
#include <vector>

#include "OHFolder.h"
#include "OHLVContainer.h"

#include <TBrowser.h>
#include <TPad.h>

class OHObject : public OHFolder
{
  private:
    int tag_;
    std::string options_;

#ifndef __MAKECINT__
    std::shared_ptr<TObject> old_object_;
    std::shared_ptr<TObject> object_;
#endif
    
    const TGPicture * GetSmallPicture( ) const;
    const TGPicture * GetLargePicture( ) const;
    void UpdateTitle(	const char * name,
                        const std::vector<std::pair<std::string,std::string> > & annotations );
    
  public:
    OHObject(	const std::string & name, 
    		    const OWLTime & time,
                OHEntry::Type type,
                int tag,
                OHFolder * parent );
        
    Bool_t IsFolder() const { return kFALSE; }    
    int GetTag() const { return tag_; }
    Bool_t CommandsAllowed() const { return kTRUE; }
    
    TObject * GetObject() const { return object_.get(); }
    void DrawObject( TPad & pad ) const;

#ifndef __MAKECINT__
    void UpdateObject(	std::unique_ptr<TObject> & object, int tag,
    			        const std::vector<std::pair<std::string,std::string> > & annotations,
                        TPad & pad );
#endif
    
    void SaveToFile( TGMainFrame * frame, const std::string & mask = "" ) const;
    void SendCommand( TGMainFrame * frame, const std::string & command ) const;   
    void Browse( TBrowser * browser );
    
    //    ClassDef( OHObject, 1 )
};

#endif
