#include <mutex>

#include "OHCanvas.h"
#include "OHPartition.h"

#include <oh/OHSubscriber.h>
#include <oh/OHRootReceiver.h>

#include <TList.h>
#include <TTimer.h>

OHCanvas * OHCanvas::s_last;

class OHPadHelper
{
  protected :
    OHPadHelper( OHCanvas & parent )
    {
    	parent.cd();
    }
};

class OHPad : public OHRootReceiver,
	      public OHPadHelper,
              public TPad,
              public TTimer
{
  public:
    OHPad( const OHObject & object, OHCanvas & parent );
    ~OHPad();
    
    void Resize( int total_number, int pos );

  private:
    TPad * Pick(Int_t px, Int_t py, TObjLink *&pickobj)
    {
        std::scoped_lock lock( mutex_ );
        if ( disabled_ )
        {
            pickobj = 0;
            return this;
        }
        
        return TPad::Pick( px, py, pickobj );
    }
    
    Bool_t Notify()
    {
        std::scoped_lock lock( mutex_ );
        object_.UpdateObject( updated_object_, tag_, annotations_, *this );
        update();
        TurnOff();
        disabled_ = false;
        return kTRUE;
    }
            
    void update();
    
    void receive( OHRootHistogram & h );

    void receive( OHRootGraph & g );

    void receive( OHRootGraph2D & g2 );

  private:    
    OHObject object_;
    OHSubscriber subscriber_;
    static std::mutex mutex_;
    std::unique_ptr<TObject> updated_object_;
    int tag_;
    std::vector<std::pair<std::string,std::string> > annotations_;
    bool disabled_;
};

std::mutex OHPad::mutex_;

OHPad::OHPad( const OHObject & object, OHCanvas & parent )
  : OHPadHelper( parent ),
    TPad( "", "", 0., 0., 1., 1. ),
    TTimer( 0, kTRUE ),
    object_( object ),
    subscriber_( IPCPartition( object_.GetPartition() ), object_.GetServer(), *this ),
    tag_(-1),
    disabled_( false )
{  
    TPad::SetBit(kCanDelete, false);
    object_.DrawObject( *this );
    TPad::Draw();
    
    try {
    	subscriber_.subscribe( std::string( object_.GetProvider() ), std::string( object_.GetName() ) );
    }
    catch ( ers::Issue & ex ) {
        ERS_DEBUG( 0, ex );
    }
}

OHPad::~OHPad()
{
    try {
    	subscriber_.unsubscribe( std::string( object_.GetProvider() ), std::string( object_.GetName() ) );
    }
    catch ( ers::Issue & ex ) {
        ERS_DEBUG( 0, ex );
    }
    Close();
}

void
OHPad::Resize( int total_number, int pos )
{
    int NumberOfColumns = (int)::sqrt( total_number );
    if ( NumberOfColumns * NumberOfColumns != total_number )
    	NumberOfColumns++;
    
    int row = pos/NumberOfColumns;
    int col = pos%NumberOfColumns;
    float height = 1./(float)((total_number-1)/NumberOfColumns+1);
    float width  = total_number < NumberOfColumns ?
            1./(float)total_number : 1./(float)NumberOfColumns;
    SetPad( col*width, 1. - (row+1)*height, (col + 1)*width, 1. - row*height );
    
    update();
}

void
OHPad::update()
{
    Modified( kTRUE );
    Update();
}

void 
OHPad::receive( OHRootHistogram & h )
{
    std::scoped_lock lock( mutex_ );
    updated_object_.reset(h.histogram.release());
    tag_ = h.tag;
    annotations_ = h.annotations;
    disabled_ = true;
    TurnOn( );  //reset the timer
}

void
OHPad::receive( OHRootGraph & g )
{
    std::scoped_lock lock( mutex_ );
    updated_object_.reset(g.graph.release());
    tag_ = g.tag;
    annotations_ = g.annotations;
    disabled_ = true;
    TurnOn( );  //reset the timer
}

void
OHPad::receive( OHRootGraph2D & g2 )
{
    std::scoped_lock lock( mutex_ );
    updated_object_.reset(g2.graph.release());
    tag_ = g2.tag;
    annotations_ = g2.annotations;
    disabled_ = true;
    TurnOn( );  //reset the timer
}

///////////////////////////////////////////////////////////////////////////////////////
//
// OHCanvas class
//
///////////////////////////////////////////////////////////////////////////////////////

OHCanvas::OHCanvas( )
{
    s_last = this;
}

OHCanvas::OHCanvas( const OHObject & object )
{
    s_last = this;
    pads_.push_back( std::shared_ptr<OHPad>( new OHPad( object, *this ) ) );
}

OHCanvas::~OHCanvas()
{
    if ( this == s_last )
    {
    	s_last = 0;
    }
}

OHCanvas * 
OHCanvas::GetLast( )
{    
    if ( !s_last || !s_last->GetCanvasImp())
    {
        delete s_last;
        s_last = new OHCanvas( );
    }
        
    return s_last;
}

void
OHCanvas::DrawObject( const OHObject & object, OHRootBrowser::ECanvasMode canvas_mode )
{
    OHCanvas * canvas;
    if ( canvas_mode == OHRootBrowser::kNewCanvas )
    {
        canvas = new OHCanvas( object );
    }
    else if ( canvas_mode == OHRootBrowser::kSingleCanvas )
    {
        canvas = OHCanvas::GetLast( );
    	canvas->Rebuild( object );
    }
    else 
    {
        canvas = OHCanvas::GetLast( );
        canvas->AddPad( object );
    }        
    
    std::string title = std::string( object.GetProvider() ) + "."
            + std::string( object.GetObject()->GetName() );
    canvas->SetTitle( title.c_str() );
}

void
OHCanvas::Rebuild( const OHObject & object )
{
    pads_.clear();
    GetListOfPrimitives()->Clear( );
    pads_.push_back( std::shared_ptr<OHPad>( new OHPad( object, *this ) ) );
    Modified(kTRUE);
    Update();
}

void
OHCanvas::AddPad( const OHObject & object )
{    
    pads_.push_back( std::shared_ptr<OHPad>( new OHPad( object, *this ) ) );
    for ( size_t i = 0; i < pads_.size(); i++ )
    {
        pads_[i]->Resize( pads_.size(), i );
    }
    Modified(kTRUE);
    Update();
}
