/*
  OHSystem.h

  Serguei Kolos April 2007
*/

#ifndef _OH_SYSTEM_H_
#define _OH_SYSTEM_H_

#include "OHFolder.h"

#include <TBrowser.h>
#include <TObjArray.h>

class OHSystem: public OHFolder 
{
    const TGPicture * GetSmallPicture() const;
    const TGPicture * GetLargePicture() const;
  
 public:
    OHSystem( ) 
      : OHFolder( "Histogram Repository", 0, false )
    { ; }
        
    Bool_t IsFolder() const { return kTRUE; }
    int GetTag() const { return 0; }
    void Browse( TBrowser * browser );
    void SaveToFile( TGMainFrame * frame, const std::string & mask = "" ) const;
    void SendCommand( TGMainFrame * , const std::string & ) const { ; }

    //    ClassDef(OHSystem,1)
};

#endif
