#ifndef __OHDisplayGuiFactory__
#define __OHDisplayGuiFactory__

/////////////////////////////////////////////////////////////
// OHDisplayGuiFactory: helper class for THistBrowser.
//
// (c) Monika Barczyk and Piotr Golonka, 
//  April 2002
//
/////////////////////////////////////////////////////////////

#include "TRootGuiFactory.h"

class OHDisplayGuiFactory: public TRootGuiFactory
{
    TGuiFactory * old_factory;
    
    TBrowserImp* CreateBrowserImp(TBrowser *b, const char *title, UInt_t width, UInt_t height, const Option_t * = 0);
    TBrowserImp* CreateBrowserImp(TBrowser *b, const char *title, Int_t x, Int_t y,UInt_t width, UInt_t height, const Option_t * = 0);
     
  public:
    OHDisplayGuiFactory();
    ~OHDisplayGuiFactory();

    //    ClassDef(OHDisplayGuiFactory,1) // My Gui Factory
};

#endif // __OHDisplayGuiFactory__
