/*
 * is_conversion.cpp
 *
 *  Created on: June 24, 2021
 *      Author: kolos
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE swrod_sample
#define BOOST_TEST_MAIN

#include <cmath>
#include <vector>

#include <boost/test/unit_test.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <TH1.h>
#include <TEfficiency.h>

#include <oh/core/EfficiencyData.h>
#include <oh/core/HistogramTraits.h>
#include <oh/OHRootProvider.h>

namespace internal {
    void convert(const TEfficiency & eff, oh::EfficiencyData & is_object);
    TEfficiency * convert(const std::string& name, const oh::EfficiencyData & is_object);

    template <class T>
    TH1 * convert(const std::string& name, const oh::HistogramData<T> & is_object);
    TH1 * convert(const std::string& name, const oh::ProfileData & is_object);
}

void fill(TH1 & h, double max, int bins)
{
    for (int i=0; i < bins; ++i) {
        h.SetBinContent(i, max);
        max -= 1;
    }
}

const int bins_number = 100;
const int total_max = 1000.;
const int passed_max = 500.;

template <class H1, class H2>
void compare(const H1 & h1, const H2 & h2) {
    BOOST_CHECK(not strcmp(h1.GetTitle(), h2.GetTitle()));
    BOOST_CHECK(h1.GetDimension() == h2.GetDimension());
    BOOST_CHECK(h1.GetEntries() == h2.GetEntries());
    BOOST_CHECK(h1.GetNbinsX() == h2.GetNbinsX());
    BOOST_CHECK(h1.GetNbinsY() == h2.GetNbinsY());
    BOOST_CHECK(h1.GetNbinsZ() == h2.GetNbinsZ());
    BOOST_CHECK(h1.GetXaxis()->GetBinWidth(1) == h2.GetXaxis()->GetBinWidth(1));
    BOOST_CHECK(h1.GetYaxis()->GetBinWidth(1) == h2.GetYaxis()->GetBinWidth(1));
    BOOST_CHECK(h1.GetZaxis()->GetBinWidth(1) == h2.GetZaxis()->GetBinWidth(1));
    BOOST_CHECK(h1.GetSize() == h2.GetSize());

    for (int i = 0; i < h1.GetSize(); ++i) {
        BOOST_CHECK(h1.GetArray()[i] == h2.GetArray()[i]);
    }

    BOOST_REQUIRE((size_t)oh::HistogramTraits<H1>::StatsSize ==
            (size_t)oh::HistogramTraits<H2>::StatsSize);

    std::vector<double> s1(100, std::nan(""));
    h1.GetStats(&s1[0]);
    BOOST_CHECK(not std::isnan(s1[oh::HistogramTraits<H1>::StatsSize-1]));
    BOOST_CHECK(std::isnan(s1[oh::HistogramTraits<H1>::StatsSize]));

    std::vector<double> s2(100, std::nan(""));
    h2.GetStats(&s2[0]);
    BOOST_CHECK(not std::isnan(s2[oh::HistogramTraits<H2>::StatsSize-1]));
    BOOST_CHECK(std::isnan(s2[oh::HistogramTraits<H2>::StatsSize]));

    for (int i = 0; i < oh::HistogramTraits<H1>::StatsSize; ++i) {
        BOOST_CHECK(s1[i] == s2[i]);
    }
}

template <class T>
void test_histogram_T(const T & histogram) {
    fill(const_cast<T&>(histogram), total_max, bins_number);

    auto is_object = OHRootProvider::convert(histogram);
    BOOST_REQUIRE(is_object);

    T * received = dynamic_cast<T*>(internal::convert("H",
            *dynamic_cast<typename oh::HistogramTraits<T>::ISType*>(is_object.get())));
    is_object.reset();
    BOOST_REQUIRE(received);

    compare(histogram, *received);
}

BOOST_AUTO_TEST_CASE(test_efficiency)
{
    BOOST_TEST_MESSAGE("Test TEfficiency conversion to/from IS");

    TH1F total("Total", "Total", bins_number, -2, 2);
    fill(total, total_max, bins_number);

    TH1F passed("Passed", "Passed", bins_number, -2, 2);
    fill(passed, passed_max, bins_number);

    TEfficiency * original = new TEfficiency(passed, total);
    original->SetNameTitle("Name", "Title");
    original->SetBetaAlpha(0.15);
    original->SetBetaBeta(0.25);
    original->SetConfidenceLevel(0.35);
    original->SetWeight(0.45);
    original->SetBetaBinParameters(10, 0.55, 0.65);

    oh::EfficiencyData is_object;
    internal::convert(*original, is_object);

    TEfficiency * received = internal::convert("efficiency", is_object);

    BOOST_CHECK(original->GetBetaAlpha() == received->GetBetaAlpha());
    BOOST_CHECK(original->GetBetaBeta() == received->GetBetaBeta());
    BOOST_CHECK(original->GetConfidenceLevel() == received->GetConfidenceLevel());
    BOOST_CHECK(original->GetWeight() == received->GetWeight());
    BOOST_CHECK(original->GetBetaAlpha(10) == received->GetBetaAlpha(10));
    BOOST_CHECK(original->GetBetaBeta(10) == received->GetBetaBeta(10));

    const TH1F * h1 = dynamic_cast<const TH1F*>(original->GetTotalHistogram());
    const TH1D * h2 = dynamic_cast<const TH1D*>(received->GetTotalHistogram());
    compare(*h1, *h2);

    h1 = dynamic_cast<const TH1F*>(original->GetPassedHistogram());
    h2 = dynamic_cast<const TH1D*>(received->GetPassedHistogram());
    compare(*h1, *h2);

    delete received;
}

BOOST_AUTO_TEST_CASE(test_histogram)
{
    BOOST_TEST_MESSAGE("Test TH1 conversion to/from IS");

    test_histogram_T(TH1C("H", "H", bins_number, -2, 2));
    test_histogram_T(TH1S("H", "H", bins_number, -2, 2));
    test_histogram_T(TH1I("H", "H", bins_number, -2, 2));
    test_histogram_T(TH1F("H", "H", bins_number, -2, 2));
    test_histogram_T(TH1D("H", "H", bins_number, -2, 2));
    test_histogram_T(TProfile("H", "H", bins_number, -2, 2));

    test_histogram_T(TH2C("H", "H", bins_number, -2, 2, bins_number, -4, 4));
    test_histogram_T(TH2S("H", "H", bins_number, -2, 2, bins_number, -4, 4));
    test_histogram_T(TH2I("H", "H", bins_number, -2, 2, bins_number, -4, 4));
    test_histogram_T(TH2F("H", "H", bins_number, -2, 2, bins_number, -4, 4));
    test_histogram_T(TH2D("H", "H", bins_number, -2, 2, bins_number, -4, 4));
    test_histogram_T(TProfile2D("H", "H", bins_number, -2, 2, bins_number, -4, 4));

    test_histogram_T(TH3C("H", "H", bins_number, -2, 2, bins_number, -4, 4, bins_number, -6, 6));
    test_histogram_T(TH3S("H", "H", bins_number, -2, 2, bins_number, -4, 4, bins_number, -6, 6));
    test_histogram_T(TH3I("H", "H", bins_number, -2, 2, bins_number, -4, 4, bins_number, -6, 6));
    test_histogram_T(TH3F("H", "H", bins_number, -2, 2, bins_number, -4, 4, bins_number, -6, 6));
    test_histogram_T(TH3D("H", "H", bins_number, -2, 2, bins_number, -4, 4, bins_number, -6, 6));
    test_histogram_T(TProfile3D("H", "H", bins_number, -2, 2, bins_number, -4, 4, bins_number, -6, 6));
}
