/*
 root_provider -> cxx

 A simple utility which can publish root histograms from a file or
 a randomly generated histogram to the OH.

 Fredrik Sandin, Nov 2001
 Monika Braczyk, Sep 2002
 Herbert Kaiser, Aug 2006  (added DrawOption)
 */

#include <iostream>
using namespace std;

// ROOT include files
#include "TEfficiency.h"
#include "TH2.h"
#include "TH3.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TProfile3D.h"
#include "TKey.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TApplication.h"
#include "TClass.h"
#include "TF3.h"

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <owl/timer.h>

// OH include files
#include <oh/OHRootProvider.h>
#include <oh/OHRootReceiver.h>

#include <map>

long search_directory(TDirectory *d, std::map<std::string, TObject*> &ld, std::string basename = "",
        int spaces = 0) {
    std::cout << "Searching for histograms in the file..." << std::endl;
    std::string space;

    for (int i = 0; i < spaces; i++)
        space += " ";

    long count = 0;
    TIter it(d->GetListOfKeys());
    TKey *key;
    TObject *obj;
    while ((key = (TKey*) it())) {
        obj = d->Get(key->GetName());
        std::cout << space << "Found an object with name " << key->GetName() << " and type "
                << obj->IsA()->GetName() << "... " << std::endl;
        if (obj->IsA()->InheritsFrom(TH1::Class())) {
            std::string name = basename + "/" + obj->GetName();
            ld[name] = obj;
            ++count;
        } else if (obj->IsA()->InheritsFrom(TDirectory::Class())) {
            count += search_directory((TDirectory*) obj, ld,
                    std::string(basename + "/" + obj->GetName()), spaces + 2);
        }
    }
    return count;
}

int publishFromFile(OHRootProvider *provider, const char *filename, int strip, int tag) {
    TFile file(filename);
    if (!file.IsOpen()) {
        std::cerr << "Unable to open the '" << filename << "' file" << endl;
        return 1;
    }

    TIter it(file.GetListOfKeys());
    std::map<std::string, TObject*> objects;
    long count = search_directory((TDirectory*) &file, objects);
    for (std::map<std::string, TObject*>::const_iterator it = objects.begin(); it != objects.end();
            ++it) {
        TObject *obj = it->second;
        std::cout << "Reading " << it->first << " object ";
        if (obj->IsA()->InheritsFrom(TH1::Class())) {

            int toStrip = strip;
            int pos = 0;

            // Stripping
            while (toStrip) {
                std::string::size_type cur;
                if ((cur = (it->first).find('/', pos + 1)) != std::string::npos) {
                    pos = cur;
                    toStrip--;
                } else {
                    break;
                }
            }

            std::string name = ((strip) ? it->first.substr(pos) : it->first);
            std::cout << "publishing it with the '" << name << "' name ... ";
            std::vector<std::pair<std::string, std::string> > ann;
            try {
                provider->publish(*(TH1*) obj, name, tag, ann);
                std::cout << "done" << std::endl;
            } catch (daq::oh::Exception &ex) {
                ers::error(ex);
                count--;
            }
        } else {
            std::cout << " - object is not a histogram, skipping it" << std::endl;
            count--;
        }
    }
    std::cout << count << " histograms has been published to the OH service" << std::endl;
    return 0;
}

void set_labels(TAxis *axis) {
    if (!axis)
        return;

    for (int i = 1; i < axis->GetNbins(); ++i) {
        char label[32];
        sprintf(label, "label %d", i);
        axis->SetBinLabel(i, label);
    }
}

void setupObject(TH1 *h, bool add_labels, int entries, int bins) {
    static TF3 f3("f3", "sin(x) + cos(y) + z", -2, 2, -4, 4, -6, 6);

    h->FillRandom("f3", entries);

    h->GetXaxis()->SetTitle("X Axis");
    h->GetYaxis()->SetTitle("Y Axis");
    h->GetZaxis()->SetTitle("Z Axis");
    if (add_labels) {
        set_labels(h->GetXaxis());
        set_labels(h->GetYaxis());
        set_labels(h->GetZaxis());
    }
}

int main(int argc, char **argv) {

    IPCCore::init(argc, argv);

    //
    // Get command line arguments
    CmdArgStr partition_name('p', "partition", "partition_name", "Partition name", CmdArg::isREQ);
    CmdArgStr server_name('s', "server", "server_name", "OH (IS) Server name", CmdArg::isREQ);
    CmdArgStr provider_name('n', "provider", "provider_name", "Histogram provider name",
            CmdArg::isREQ);
    CmdArgStr histogram_name('h', "histogram", "histogram_name", "Histogram type name",
            CmdArg::isREQ);
    CmdArgStr option('o', "option", "option",
            "1d, 2d, 3d, 1p, 2p, 3p, 1g, 2g, 1e, 2e, 3e or the name of a ROOT file", CmdArg::isREQ);
    CmdArgInt tag('t', "tag", "object-tag", "publish histogram with this tag");
    CmdArgInt bins_number('b', "bins", "bins-number", "number of bins for histogram");
    CmdArgBool graphics('g', "graphics", "display histograms in graphics mode.");
    CmdArgBool verbose('v', "verbose", "print histogram content.");
    CmdArgBool labels('l', "labels", "add labels to histograms.");
    CmdArgStr drawoption('d', "drawoption", "drawoption",
            "give a string for the ROOT DrawOption for the histogram");
    CmdArgInt strip('e', "strip", "strip", "strips n tokens from the histogram name");
    CmdArgInt entries('E', "entries", "entries", "number of entries in the histogram");
    CmdArgInt number('N', "number", "histograms-number", "number of histogram to be published");

    CmdLine cmd(*argv, &partition_name, &server_name, &provider_name, &histogram_name, &option,
            &tag, &graphics, &verbose, &labels, &drawoption, &strip, &bins_number, &entries, &number, NULL);

    tag = -1;
    strip = 0;
    entries = 10000;
    bins_number = 10;
    number = 1;
    CmdArgvIter arg_iter(--argc, ++argv);
    cmd.description("OH histogram publisher utility");
    cmd.parse(arg_iter);

    TApplication theApp("App", 0, 0);

    //
    // Create a ROOT provider
    IPCPartition p(partition_name);
    OHRootProvider *provider = 0;
    try {
        provider = new OHRootProvider(p, (const char*) server_name, (const char*) provider_name);
    } catch (daq::oh::Exception &ex) {
        ers::fatal(ex);
        return 1;
    }

    TNamed *histo = 0;

    // create DrawOption annotation if wanted
    std::vector<std::pair<std::string, std::string> > ann;
    if (drawoption.isNULL())
        ann = oh::util::EmptyAnnotation;
    else {
        ann.resize(1);
        ann[0].first = "FINAL";
        ann[0].second = "FINAL";
    }

    for (int i = 0; i < number; ++i) {
        std::string name((const char*)histogram_name);
        if (number > 1) {
            name += std::to_string(i);
        }
        
        // Create and publish the histogram(s)
        string opt((const char*) option);
        if (opt == "1d") {
            TH1F *h = new TH1F(name.c_str(), name.c_str(), bins_number, -2, 2);
            setupObject(h, labels, entries, bins_number);
            histo = h;
        } else if (opt == "2d") {
            TH2F *h = new TH2F(name.c_str(), name.c_str(), sqrt((double) bins_number), -2, 2,
                    sqrt((double) bins_number), -4, 4);
            setupObject(h, labels, entries, bins_number);
            histo = h;
        } else if (opt == "3d") {
            TH3F *h = new TH3F(name.c_str(), name.c_str(), pow((double) bins_number, 0.33), -2, 2,
                    pow((double) bins_number, 0.33), -4, 4, pow((double) bins_number, 0.33), -6, 6);
            setupObject(h, labels, entries, bins_number);
            histo = h;
        } else if (opt == "1p") {
            TProfile *h = new TProfile(name.c_str(), name.c_str(), 10, -200, 200);
            setupObject(h, labels, entries, bins_number);
            histo = h;
        } else if (opt == "2p") {
            TProfile2D *h = new TProfile2D(name.c_str(), name.c_str(), 10, -200, 200, 20, -100,
                    100);

            setupObject(h, labels, entries, bins_number);
            histo = h;
        } else if (opt == "3p") {
            TProfile3D *h = new TProfile3D(name.c_str(), name.c_str(), 10, -200, 200, 20, -100, 100,
                    30, -100, 100);

            setupObject(h, labels, entries, bins_number);
            histo = h;
        } else if (opt == "1g") {
            TH1F *h = new TH1F(name.c_str(), name.c_str(), bins_number, -2, 2);
            setupObject(h, labels, entries, bins_number);
            histo = new TGraph(h);
            delete h;
        } else if (opt == "2g") {
            TH2F *h = new TH2F(name.c_str(), name.c_str(), sqrt((double) bins_number), -2, 2,
                    sqrt((double) bins_number), -4, 4);
            setupObject(h, labels, entries, bins_number);
            histo = new TGraph2D(h);
            delete h;
        } else if (opt == "1e") {
            TH1F t("total", "total", bins_number, -2, 2);
            setupObject(&t, labels, entries, bins_number);
            TH1F p("passed", "passed", bins_number, -2, 2);
            setupObject(&p, labels, entries / 100, bins_number);
            histo = new TEfficiency(p, t);
            histo->SetNameTitle(name.c_str(), name.c_str());
        } else if (opt == "2e") {
            TH2F t("total", "total", sqrt((double) bins_number), -2, 2, sqrt((double) bins_number),
                    -4, 4);
            setupObject(&t, labels, entries, bins_number);
            TH2F p("passed", "passed", sqrt((double) bins_number), -2, 2,
                    sqrt((double) bins_number), -4, 4);
            setupObject(&p, labels, entries / 100, bins_number);
            histo = new TEfficiency(p, t);
            histo->SetNameTitle(name.c_str(), name.c_str());
        } else if (opt == "3e") {
            TH3F t("total", "total", pow((double) bins_number, 0.33), -2, 2,
                    pow((double) bins_number, 0.33), -4, 4, pow((double) bins_number, 0.33), -6, 6);
            setupObject(&t, labels, entries, bins_number);
            TH3F p("passed", "passed", pow((double) bins_number, 0.33), -2, 2,
                    pow((double) bins_number, 0.33), -4, 4, pow((double) bins_number, 0.33), -6, 6);
            setupObject(&p, labels, entries / 100, bins_number);
            histo = new TEfficiency(p, t);
            histo->SetNameTitle(name.c_str(), name.c_str());
        } else {
            return publishFromFile(provider, opt.c_str(), strip, tag);
        }

        if (verbose) {
            histo->Print("all");
        }

        try {
            if (histo->InheritsFrom(TH1::Class()))
                provider->publish(*static_cast<TH1*>(histo), name.c_str(), tag, ann);
            else if (histo->InheritsFrom(TGraph::Class()))
                provider->publish(*static_cast<TGraph*>(histo), name.c_str(), tag, ann);
            else if (histo->InheritsFrom(TGraph2D::Class()))
                provider->publish(*static_cast<TGraph2D*>(histo), name.c_str(), tag, ann);
            else if (histo->InheritsFrom(TEfficiency::Class()))
                provider->publish(*static_cast<TEfficiency*>(histo), name.c_str(), tag, ann);
        } catch (daq::oh::Exception &ex) {
            ers::error(ex);
            return 1;
        }
    }

    std::cout << "publish successful " << std::endl;
    delete provider;

    if (graphics) {
        cout << "Entering ROOT message loop..." << endl
                << "Click 'Quit ROOT' on the file menu to quit" << endl;
        TCanvas *canvas = new TCanvas(histo->GetName(), histo->GetTitle());
        if (drawoption.isNULL())
            histo->Draw();
        else
            histo->Draw((const char*) drawoption);
        canvas->Update();
        theApp.Run();
    }
    delete histo;
}
