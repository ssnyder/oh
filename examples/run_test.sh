#!/bin/sh
####################################################################
#
#	Test Script for the OH package
#	Created by Sergei Kolos; 03.12.2001
#
####################################################################

# This will make core file in case of fatal error
start_partition_command=`echo $0 | sed -e 's/\/run_test.sh/\/start_partition.sh/g'`
$start_partition_command
$start_partition_command OHTest

ref="server.ref"

is_server="is_server -n Histo -p OHTest"
oh_test="./oh_test_iterator -p OHTest -s Histo -P Root"
oh_provider="./oh_raw_provider -p OHTest -s Histo -n Raw -h Test"
oh_provider1="./oh_root_provider -p OHTest -s Histo -n Root -h Test -o 1d"
oh_provider2="./oh_root_provider -p OHTest -s Histo -n Root -h Test -o 1d -t 1000"
oh_provider3="./oh_graph_provider -p OHTest -s Histo -n Root -h Test -o 1d -t 1000"


if test $1"XXX" != "XXX"
then
	ROOTSYS=$1
	export ROOTSYS
	echo $ROOTSYS
	LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH
	export LD_LIBRARY_PATH
	echo $LD_LIBRARY_PATH
	ldd ./oh_root_receiver
	oh_receiver="./oh_root_receiver -p OHTest -s Histo"
else
	oh_receiver="./oh_ls -p OHTest -s Histo"
fi

echo Histogram IS server command is \"$is_server\"
echo Histogram Raw Provider command is \"$oh_provider\"
echo Histogram Root Provider commands are \"$oh_provider1\" \"$oh_provider2\"
echo Graph Provider command is \"$oh_provider3\"
echo Histogram Receiver command is \"$oh_receiver\"

stop_server()
{
    if test $1 -ne 0
    then
	kill $1
    fi
}
trap stop_server 1 2 3 4 5 6 7 8 10 12 13 14 15

run_server()
{
	echo
	echo "Waiting for the Histogram $1 server to start up... "
	rm -f $ref
	$2 > $ref&

	count=0
	while test ! -s $ref -a $count -lt 18
	do
    		sleep 1
    		count=`expr $count + 1`
	done

	if test ! -s $ref
	then
   		echo "Failed!"
    		echo "(Server was not started)"
    		exit 1
	else
    		echo "OK!"
		rm $ref
	fi
}

echo "###########################################################"
echo "Histograming test started at `date`"
echo "###########################################################"

#################################
# Start IS server
#################################
run_server "IS" "$is_server"
isserver_id=$!

sleep 2
#################################
# Start Iterator test
#################################
echo "Executing OHIterator test \"$oh_test\""
$oh_test || { echo "ERROR: OH check failed" ; exit 1 ; }

sleep 2
#################################
# Start Provider
#################################
echo "Executing Histogram Provider \"$oh_provider\""
$oh_provider || { echo "ERROR: OH check failed" ; exit 1 ; }

#################################
# Start Receiver
#################################
echo "Executing Histogram Receiver \"$oh_receiver\""
$oh_receiver || { echo "ERROR: OH check failed" ; exit 1 ; }

echo "OK!"

sleep 2
#################################
# Start Provider
#################################
echo "Executing Histogram Provider \"$oh_provider1\""
$oh_provider1 || { echo "ERROR: OH check failed" ; exit 1 ; }

#################################
# Start Receiver
#################################
echo "Executing Histogram Receiver \"$oh_receiver\" "
$oh_receiver || { echo "ERROR: OH check failed" ; exit 1 ; }

echo "OK!"

sleep 2
#################################
# Start Provider
#################################
echo "Executing Histogram Provider \"$oh_provider2\" "
$oh_provider2 || { echo "ERROR: OH check failed" ; exit 1 ; }

#################################
# Start Receiver
#################################
echo "Executing Histogram Receiver ... "
$oh_receiver || { echo "ERROR: OH check failed" ; exit 1 ; }

echo "OK!"

sleep 2
#################################
# Start Provider
#################################
echo "Executing Graph Provider \"$oh_provider3\" "
$oh_provider2 || { echo "ERROR: OH check failed" ; exit 1 ; }

#################################
# Start Receiver
#################################
echo "Executing Histogram Receiver ... "
$oh_receiver || { echo "ERROR: OH check failed" ; exit 1 ; }

echo "OK!"

#################################
# Stopping everything
#################################
echo "Stopping everything..."
ipc_rm -f -t -n ".*" -i ".*"
sleep 5
echo "OK!"

