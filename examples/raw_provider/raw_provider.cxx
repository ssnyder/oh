/*
    raw_provider.cxx
    
    A RAW provider example.
    
    Fredrik Sandin, Nov 2001

*/

#include <iostream>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ipc/signal.h>

#include <oh/OHRawProvider.h>

using namespace std;

class MyCommandListener : public OHCommandListener
{
    public:
    	void command ( const std::string & name, const std::string & cmd )
	{
	    std::cout << " Command " << cmd << " received for the " << name << " histogram" << std::endl;
	}
	
    	void command ( const std::string & cmd )
	{
	    std::cout << " Command " << cmd << " received for all histograms" << std::endl;
	    if ( cmd == "exit" )
	    {
	    	daq::ipc::signal::raise();
	    }
	}
};

int main( int argc, char ** argv )
{
    IPCCore::init( argc, argv );
    
    //
    // Get parameters from the command line
    CmdArgStr partition_name( 'p', "partition", "partition_name",
                              "Partition name", CmdArg::isREQ );
    CmdArgStr server_name( 's', "server", "server_name",
                           "OH (IS) Server name", CmdArg::isREQ );
    CmdArgStr provider_name( 'n', "provider", "provider_name",
                              "Histogram provider name", CmdArg::isREQ );
    CmdArgStr histogram_name( 'h', "histogram", "histogram_name",
                              "Histogram type name", CmdArg::isREQ );
    CmdArgBool     update('u', "update", "publish histograms with update mod");
    CmdArgBool     wait('w', "wait", "wait for commands");
    
    CmdLine cmd( *argv, &partition_name, &server_name,
                 &provider_name, &histogram_name, &update, &wait, NULL );
    
    CmdArgvIter arg_iter( --argc, ++argv );
    
    cmd.description( "OH RAW provider example" );
    
    wait = false;
    
    cmd.parse(arg_iter);
    
    //
    // Create an OHRawProvider and publish some sample histograms
    /////////////////////////////////////////////////////////////////////////
    
    IPCPartition partition( partition_name );
    MyCommandListener lst;
    
    // Here we choose the source format of the histogram data with
    // the template arguments. Lets pretend we have 16-bit bin heights
    // with 8-bit errors and floating point axes.
    OHRawProvider<> * raw = 0;
    try
    {
    	raw = new OHRawProvider<>( partition, (const char*)server_name, (const char*)provider_name, &lst );
    }
    catch( daq::oh::Exception & ex )
    {
    	ers::fatal( ex );
    }
    
    // A sample annotation which stores the origin of the histograms
    vector<pair< string,string> > ann;
    ann.push_back( make_pair( "Source", "OH RAW Provider example application" ) );
    
    // Sample data, this should be retrieved from somewhere in a real app
    short contents[11] = { 100,200,300,400,500,600,700,800,900,1000,1100 };
    char  errors[11] = { 1,2,3,5,8,13,21,34,55,89,97 };
    float axis[9] = { 1,2,4,8,16,32,64,128,256 };

    // Publish a sample 1D histogram with variable width bins, errors,
    // underflow and overflow
    
    string h1_name = (const char*)histogram_name;
    h1_name += "[1d]";
    
    int n = 0;
    while( ++n < 10 )
    {
	raw -> publish(	h1_name.c_str(), "RAW Histogram 1D",
		     	OHAxis( "X Axis", 8, axis ),
		     	contents, 
		     	errors,
		     	true,
		     	update,
		     	ann );
	cout << " publishing step " << n << endl;
				
        for ( int i = 0; i < 10; i++ )
	    contents[i] += n;
    }
    
    // Publish a sample 2D histogram with variable width bins and errors
    string h2_name = (const char*)histogram_name;
    h2_name += "[2d]";
    raw -> publish( h2_name.c_str(), "RAW Histogram 2D",
		     OHMakeAxis( "X Axis", 3, axis ),
		     OHMakeAxis( "Y Axis", 3, axis ),
		     contents, 
		     errors,
		     false,
		     update,
		     ann );
    cout << " publish successfull " << endl;

    // demo part for graphs

    // Sample data, this should be retrieved from somewhere in a real app
    double y[11] = { 1.6,1.0,1.2,1.3,1.6,2.5,2.3,2.6,2.0,0.2,0.9 };
    double x[11] = { 1 , 2 , 3 , 4, 5, 6, 7, 8, 9, 10, 11 }; 
    double z[11] = { 1 , 4 , 1 , 4, 7, 6, 3, 8, 6, 8, 11 }; 
    double ex[11] = { 0.1,0.2,0.1,0.5,0.1,0.1,0.1,0.1,0.1,0.1,0.4, };
    double ey[11] = { 0.1,0.2,0.1,0.1,0.1,0.2,0.1,0.1,0.7,0.1,0.4, };
    double ez[11] = { 0.1,0.2,0.1,0.0,0.3,0.2,0.8,0.1,0.7,0.2,0.4, };
    std::vector<const double*> errorlist(2);
    errorlist[0]=ex;
    errorlist[1]=ey;

    // Publish a sample graph with errors,
    
    raw -> publish( std::string(histogram_name)+"[Graph]", "RAW Graph wigh Errors",
			11,
			x,
			y,
		     	OHMakeAxis<double>( "X Axis", 11,0.0,1),
		     	OHMakeAxis<double>( "Y Axis", 30,0.0,0.1 ),
			oh::Graph::Symmetric,
			errorlist,
		     	update,
		     	ann );

    errorlist.resize(3);
    errorlist[2]=ez;
    raw -> publish( std::string(histogram_name)+"[Graph2D]", "RAW Graph wigh Errors",
			11,
			x,
			z,
			y,
		     	OHMakeAxis<double>( "X Axis", 11,0.0,1),
		     	OHMakeAxis<double>( "Y Axis", 11,0.0,1),
		     	OHMakeAxis<double>( "Z Axis", 30,0.0,0.1 ),
			oh::Graph2D::Symmetric,
			errorlist,
		     	update,
		     	ann );
		 
    if ( wait ) {
    	daq::ipc::signal::wait_for();
    }
}
