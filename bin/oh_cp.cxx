/*
    oh_cp.cxx
    
    A simple utility which copies histograms from OH to a ROOT file.
    
    Serguei Kolos, Mar 2007

*/

#include <iostream>
#include <sstream>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <rc/RunParams.h>

#include <oh/OHFileSaver.h>
#include <oh/OHProviderIterator.h>
#include <oh/OHServerIterator.h>
#include <oh/OHStream.h>

namespace
{
#if defined( OH_MOVE )
    const char * DESCRIPTION = "Moves objects (histograms and graphs) from OH to a given ROOT file";
    const bool   REMOVE = true;
#elif defined( OH_COPY )
    const char * DESCRIPTION = "Copies objects (histograms and graphs) from OH to a given ROOT file";
    const bool   REMOVE = false;
#else
#error "Either OH_MOVE or OH_COPY macro must be defined"
#endif
}

template <class Stream>
int 
save_objects(	const IPCPartition & partition, 
		const std::string & server_name, 
		const std::string & provider_name,
		const std::string & object_name,
		const std::string & type_name,
                int history_depth,
		OHFileSaver & fs )
{
    try
    {
	Stream in( partition, server_name, provider_name, object_name, false, history_depth );

	std::cout << " saving " << type_name << "(s) from the '" << server_name 
        	<< "' server in partition '" << partition.name() << "' ... ";

	size_t counter = 0;
	while ( !in.eof() )
	{
	    in.retrieve( fs );
	    ++counter;
	}
	std::cout << counter << " " << type_name << "(s) saved" << std::endl;
    }
    catch( ers::Issue & ex )
    {
    	ers::error( ex );
        return 1;
    }
    return 0;
}

template <class Stream>
int 
save_all_objects(	const IPCPartition & partition, 
                        const std::string & server_criteria,
                        const std::string & provider_name,
                        const std::string & object_name,
                        const std::string & type_name,
                        int history_depth,
                        OHFileSaver & fs )
{
    std::cout << "Copying OH objects from the '" << partition.name() 
    	<< "' partition to the '" << fs.name() << "' file ..." << std::endl;
    
    int result = 0;
    try
    {
	OHServerIterator it( partition, server_criteria );

	size_t counter = 0;
	while ( it++ )
        {
            result += save_objects<Stream>( partition, it.name(), provider_name, 
            		object_name, type_name, history_depth, fs );
	    ++counter;
	}
	std::cout << counter << " server(s) saved" << std::endl;
    }
    catch( ers::Issue & ex )
    {
    	ers::error( ex );
        return 1;
    }
    catch (std::exception & e) {
    	std::cerr <<  e.what() << std::endl;
        return 1;
    }
    return result;
}

ERS_DECLARE_ISSUE( 	ers, // namespace
			BadFile, // issue class name
			"Can not open '" << file_name << "' file", // no message
                        ((std::string)file_name ) // single attribute 
                 )

int main( int argc, char ** argv )
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::fatal( ex );
        return 1;
    }

    CmdArgBool	use_offline	( 'O', "use-offline", "Use Offline convention for saving LB dependent histograms.");
    CmdArgBool	add_run_number	( 'R', "add-run-number", "Add run number to the given file name.");
    CmdArgStr	partition_name	( 'p', "partition", "partition_name", "Partition name" );
    CmdArgStr	server_name	( 's', "server", "server_name", "OH (IS) server name (regular expressions allowed)" );
    CmdArgStr	directory	( 'd', "directory", "directory_name", "directory to write file" );
    CmdArgStr	provider_name	( 'n', "provider", "provider_name", "OH provider name (regular expressions allowed)" );
    CmdArgStr	object_name	( 'o', "object", "object_name", "OH object name (regular expressions allowed)" );
    CmdArgStr	file_name	( 'f', "file", "file_name", "ROOT file name for saving objects" );
    CmdArgChar	object_type	( 't', "type", "object_type",	"OH object type\n"
				"allowed types are: 'H' - histograms, 'G' - graphs, 'E' - efficiency, 'A' - all (default)" );
    CmdArgInt	compression_level( 'c', "compression", "compression_level",
    				"Compression level fo the root file, default is 2 (speed/size optimal)" );
    CmdArgInt	run_number( 'r', "run", "run_number",
    				"Use this run number instead of retrieving it automatically" );
    CmdArgInt	history_depth( 'H', "history", "history_depth",
    				"Copy this number of last object values, default is 1, -1 means all" );
                                                         
    CmdLine cmd( *argv, &partition_name, &server_name, &provider_name, &object_name, 
    			&use_offline, &object_type, &file_name, &directory, 
                        &compression_level, &run_number, &add_run_number, &history_depth, NULL );
    CmdArgvIter arg_iter( --argc, ++argv );
    cmd.description( DESCRIPTION );
    
    add_run_number = false;
    use_offline = false;
    server_name = ".*";
    object_name = ".*";
    provider_name = ".*";
    object_type = 'A';
    history_depth = 1;
    compression_level = 2;
    run_number = 0;
    
    cmd.parse(arg_iter);
    
    IPCPartition partition;
    
    if ( partition_name.flags() && CmdArg::GIVEN )
    {
    	partition = IPCPartition( (const char*)partition_name );
    }
    
    std::string FileName;
    if ( directory.flags() && CmdArg::GIVEN )
    {
	FileName = directory;
	if ( FileName.size() && FileName[FileName.size()-1] != '/' )
        {
            FileName += '/';
        }
    }

    if (    add_run_number
        && !(run_number.flags() && CmdArg::GIVEN) )
    {
	try
	{
	    ISInfoDictionary id( partition );
	    RunParams runParameter;
	    runParameter.run_number = 0;
	    id.getValue("RunParams.RunParams",runParameter);
	    run_number = runParameter.run_number;
	}
	catch( daq::is::Exception & ex ) {
	    ERS_DEBUG( 1, "Run Number infomation is not available " << ex );
	}
    }

    if ( file_name.flags() && CmdArg::GIVEN )
    {
        if ( add_run_number )
        {
	    std::ostringstream out;
	    out << 'r'  << std::setw(10) << std::setfill('0') << run_number << "_";
	    FileName += out.str();
        }
        
    	FileName += file_name;        
    }
    else
    {        
	std::ostringstream out;
	out << 'r'  << std::setw(10) << std::setfill('0') << run_number << "_";
	FileName += out.str();
	FileName += partition.name();
	
        if ( server_name.flags() && CmdArg::GIVEN )
	{
	    FileName += "_";
	    FileName += server_name;
	}
	
        if ( provider_name.flags() && CmdArg::GIVEN )
	{
	    FileName += "_";
	    FileName += provider_name;
	}
        
    	FileName += ".root";
    }
    
    OHFileSaver file_saver( FileName, REMOVE, compression_level );
    
    if ( file_saver.error() )
    {
    	ers::error( ers::BadFile( ERS_HERE, FileName ) );
        return 1;
    }
    
    if ( use_offline )
    {
    	file_saver.useOfflineStyle( run_number );
    }
    
    switch( object_type )
    {
	case 'A':
	    return save_all_objects<OHStream>( partition, 
                             (const char*)server_name,
                             (const char*)provider_name,
                             (const char*)object_name,
                             "object",
                             history_depth,
                             file_saver );
        case 'E':
            return save_all_objects<OHEfficiencyStream>( partition,
                              (const char*)server_name,
                              (const char*)provider_name,
                              (const char*)object_name,
                              "efficiency",
                              history_depth,
                              file_saver );
        case 'G':
            return save_all_objects<OHGraphStream>( partition,
                              (const char*)server_name,
                              (const char*)provider_name,
                              (const char*)object_name,
                              "graph",
                              history_depth,
                              file_saver );
	case 'H':
	    return save_all_objects<OHHistogramStream>( partition, 
                              (const char*)server_name,
                              (const char*)provider_name,
                              (const char*)object_name,
                              "histogram",
                              history_depth,
                              file_saver );
	default:
            break;
    }

    ers::error( daq::oh::InvalidParameter( ERS_HERE, object_type ) );
    return 1;
}
