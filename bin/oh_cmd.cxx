/*
    oh_cmd.cxx
    
    A simple utility which send a given command to OH providers.
    
    Serguei Kolos, Mar 2007

*/

#include <iostream>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <oh/OHCommandSender.h>
#include <oh/OHProviderIterator.h>
#include <oh/OHServerIterator.h>
#include <oh/OHIterator.h>

template <class Iterator>
int 
process_one_server(	const IPCPartition & partition, 
			const std::string & server_name, 
                        const std::string & provider_name,
                        const std::string & object_name,
                        const std::string & type_name,
                        const std::string & command,
                        bool verbose )
{
    try
    {
	OHProviderIterator pit( partition, server_name, provider_name );
	
        OHCommandSender commander( partition );
        
        while ( pit++ )
        {
	    if ( object_name.empty() )
	    {
		if ( pit.isActive() )
		{
		    if ( verbose ) std::cout << " sending command to the '" << pit.name( ) << " provider ... ";
		    commander.sendCommand( server_name, pit.name(), command );
		    if ( verbose ) std::cout << "done" << std::endl;
		}
		else
		{
		    std::cerr << " the '" << pit.name( ) << "' provider does not exist" << std::endl;
		}
	    }
            else
            {
		Iterator it( partition, server_name, pit.name(), object_name );
		while ( it++ ) {
		    if ( verbose ) std::cout << " sending command to the '" << it.name() << "' " << type_name 
                    				<< " published by '" << pit.name( ) << " provider ... ";
		    commander.sendCommand( server_name, pit.name(), it.name(), command );
		    if ( verbose ) std::cout << "done" << std::endl;
		}
	    }
	}
    }
    catch( ers::Issue & ex )
    {
    	ers::error( ex );
        return 1;
    }
    return 0;
}

template <class Iterator>
int 
process_all_servers(	const IPCPartition & partition, 
                        const std::string & provider_name,
                        const std::string & object_name,
                        const std::string & type_name,
                        const std::string & command,
                        bool verbose )
{
    int result = 0;
    try
    {
	OHServerIterator it( partition );

	while ( it++ )
        {
	    if ( verbose ) std::cout << " processing the '" << it.name() << "' server ... ";
            result += process_one_server<Iterator>( partition, it.name(), provider_name, object_name, type_name, command, verbose );
	    if ( verbose ) std::cout << "done" << std::endl;
	}
    }
    catch( ers::Issue & ex )
    {
    	ers::error( ex );
        return 1;
    }
    return result;
}

int main( int argc, char ** argv )
{
    try {
	std::list< std::pair< std::string, std::string > > options = IPCCore::extractOptions( argc, argv );
	options.push_front( std::make_pair( std::string("clientCallTimeOutPeriod"), std::string("1000") ) );
        IPCCore::init( options );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::fatal( ex );
        return 1;
    }

    CmdArgBool verbose ('v', "verbose", "trace execution");
    CmdArgStr  partition_name( 'p', "partition", "partition_name", "Partition name" );
    CmdArgStr  server_name( 's', "server", "server_name", "OH (IS) server name" );
    CmdArgStr  provider_name( 'n', "provider", "provider_name", "OH provider name (regular expressions allowed)" );
    CmdArgStr  object_name( 'o', "object", "object_name", "OH object name (regular expressions allowed)" );
    CmdArgStr  command( 'c', "command", "command_line", "Command to be sent", CmdArg::isREQ );
    CmdArgChar object_type( 't', "type", "object_type",	"OH object type\n"
    			"allowed types are: 'H' - histograms, 'G' - graphs, 'E' - efficiency, 'A' - all (default)" );
                                                         
    CmdLine cmd( *argv, &partition_name, &server_name, &provider_name, &object_name, &object_type, &command, &verbose, NULL );
    CmdArgvIter arg_iter( --argc, ++argv );
    cmd.description( "Send commands to OH providers" );
    
    verbose = false;
    object_name = "";
    provider_name = ".*";
    object_type = 'A';
    cmd.parse(arg_iter);
        
    IPCPartition partition;
    
    if ( partition_name.flags() && CmdArg::GIVEN )
    {
    	partition = IPCPartition( (const char*)partition_name );
    }
    
    if ( server_name.flags() && CmdArg::GIVEN )
    {
	switch( object_type )
	{
	    case 'A':
		return process_one_server<OHIterator>( partition,
		        (const char*)server_name,
                        (const char*)provider_name,
                        (const char*)object_name,
                        "object",
                        (const char*)command, verbose );
            case 'E':
                return process_one_server<OHEfficiencyIterator>( partition,
                        (const char*)server_name,
                        (const char*)provider_name,
                        (const char*)object_name,
                        "efficiency",
                        (const char*)command, verbose );
            case 'G':
                return process_one_server<OHGraphIterator>( partition,
                        (const char*)server_name,
                        (const char*)provider_name,
                        (const char*)object_name,
                        "graph",
                        (const char*)command, verbose );
	    case 'H':
		return process_one_server<OHHistogramIterator>( partition,
		        (const char*)server_name,
                        (const char*)provider_name,
                        (const char*)object_name,
                        "histogram",
                        (const char*)command, verbose );
	    default:
            	break;
	}
    }
    else
    {
	switch( object_type )
	{
	    case 'A':
		return process_all_servers<OHIterator>( partition,
		        (const char*)provider_name,
			(const char*)object_name,
			"object",
			(const char*)command, verbose );
            case 'E':
                return process_all_servers<OHEfficiencyIterator>( partition,
                        (const char*)provider_name,
                        (const char*)object_name,
                        "efficiency",
                        (const char*)command, verbose );
            case 'G':
                return process_all_servers<OHGraphIterator>( partition,
                        (const char*)provider_name,
                        (const char*)object_name,
                        "graph",
                        (const char*)command, verbose );
	    case 'H':
		return process_all_servers<OHHistogramIterator>( partition,
		        (const char*)provider_name,
                        (const char*)object_name,
                        "histogram",
                        (const char*)command, verbose );
	    default:
            	break;
	}
    }
    
    ers::error( daq::oh::InvalidParameter( ERS_HERE, object_type ) );
    return 1;
}
