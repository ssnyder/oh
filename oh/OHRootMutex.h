#ifndef OH_ROOT_MUTEX_H
#define OH_ROOT_MUTEX_H

#include <mutex>

struct OHRootMutex
{
    static std::mutex & getMutex()
    { 
	static std::mutex * mutex = new std::mutex;
	return *mutex;
    }
    
  private:
    OHRootMutex() = delete;
};

#endif
