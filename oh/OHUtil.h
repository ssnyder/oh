/*
    OHutil.h - OH private header file.
    
    Fredrik Sandin, Oct 2001
    Monika Barczyk, Feb 2003
    Sergei Kolos, Jan 2004
*/

#ifndef OH_UTILS_H
#define OH_UTILS_H

#include <string>
#include <vector>

namespace oh {
    namespace util {
	const std::vector< std::pair<std::string,std::string> > EmptyAnnotation;
	const std::vector<const double*> 			EmptyErrorList;
        
	std::string create_info_name (	const std::string & server_name,
					const std::string & provider_name,
					const std::string & histo_name );
                                        
	std::string create_regex (	const std::string & provider_name,
					const std::string & histo_name );
                                        
	std::string create_provider_name (	const std::string & server_name,
						const std::string & provider_name );
                                                
	std::string get_server_name( const std::string & s );
	
        std::string get_object_name(	const std::string & s );
        
        std::string get_histogram_name(	const std::string & s );
        
	std::string get_provider_name (	const std::string & s );
    }
}

#endif // OH_UTILS_H
