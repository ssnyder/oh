#ifndef GRAPH_H
#define GRAPH_H

#include "oh/core/Object.h"
#include "oh/core/Axis.h"
#include <string>
#include <ostream>
#include <vector>


// <<BeginUserCode>>

// <<EndUserCode>>

namespace oh
{
/**
 * Used to store simple graphs in the IS
 * 
 * @author  produced by the IS generator
 */

class Graph : public Object {
public:
    enum ErrorStyle {None,Symmetric,Asymmetric,AsymBent};


    /**
     * Optional error values for the points
     */
    std::vector<double>                        errors;

    /**
     * What are the errors: None(TGraph), Symmetric(TGraphErrors),Asymmetric(TGraphAymmErrors),AsymBent(TGraphBent)
     */
    ErrorStyle                                 errorStyle;

    /**
     * Values of all the points in the graph
     */
    std::vector<double>                        points;

    /**
     * Number of entries
     */
    uint64_t                                   entries;

    /**
     * Axes for this object
     */
    std::vector<Axis>                          axes;


    static const ISType & type() {
	static const ISType type_ = Graph( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	Object::print( out );
	out << std::endl;
	out << "errors[" << errors.size() << "]:\t// Optional error values for the points" << std::endl;
	for ( size_t i = 0; i < errors.size(); ++i )
	    out << i << " : " << errors[i] << std::endl;
	out << "errorStyle: " << errorStyle << "\t// What are the errors: None(TGraph), Symmetric(TGraphErrors),Asymmetric(TGraphAymmErrors),AsymBent(TGraphBent)" << std::endl;
	out << "points[" << points.size() << "]:\t// Values of all the points in the graph" << std::endl;
	for ( size_t i = 0; i < points.size(); ++i )
	    out << i << " : " << points[i] << std::endl;
	out << "entries: " << entries << "\t// Number of entries" << std::endl;
	out << "axes[" << axes.size() << "]:\t// Axes for this object" << std::endl;
	for ( size_t i = 0; i < axes.size(); ++i )
	    out << i << " : " << axes[i] << std::endl;
	return out;
    }

    Graph( )
      : Object( "Graph" )
    {
	initialize();
    }

    ~Graph(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    Graph( const std::string & type )
      : Object( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	Object::publishGuts( out );
	out << errors << errorStyle << points << entries << axes;
    }

    void refreshGuts( ISistream & in ){
	Object::refreshGuts( in );
	in >> errors >> errorStyle >> points >> entries >> axes;
    }

private:
    void initialize()
    {
	errorStyle = None;

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>
public:
    static const unsigned int dimension = 1;

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const Graph & info ) {
    info.print( out );
    return out;
}

}

#endif // GRAPH_H
