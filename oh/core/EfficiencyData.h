/*
 * EfficiencyData.h
 *
 *  Created on: Jul 1, 2021
 *      Author: kolos
 */

#ifndef OH_CORE_EFFICIENCYDATA_H_
#define OH_CORE_EFFICIENCYDATA_H_

#include <oh/core/Efficiency.h>

namespace oh {
    typedef ObjectBase<Efficiency> EfficiencyData;
}

#endif /* OH_CORE_EFFICIENCYDATA_H_ */
