/*
 * GraphData.h - interface to the Graph class in IS
 * Serguei Kolos
 */

#ifndef OH_GRAPH_DATA_H
#define OH_GRAPH_DATA_H

#include <oh/core/IGraph.h>
#include <oh/core/Graph.h>
#include <oh/core/Graph2D.h>

namespace oh
{
    typedef IGraph<oh::Graph>	GraphData;
    typedef IGraph<oh::Graph2D>	Graph2DData;
}

#endif //OH_GRAPH_DATA_H

