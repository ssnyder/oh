/*
 * ObjectBase.h - interface to the base Object class in IS
 * Serguei Kolos, Aug 2006
 */

#ifndef OH_CORE_OBJECT_BASE_H
#define OH_CORE_OBJECT_BASE_H

#include <oh/core/Axis.h>

namespace oh
{
    template <class T>
    struct ObjectBase : public T
    {        
	ObjectBase( )
	{ ; }
       
	ObjectBase( const std::string & class_name )
          : T( class_name )
	{ ; }
       
        /** Set name */
	void set_name(const std::string& n) {
	    this->id.name = n;
	}

	/** Get name */
	const std::string & get_name() const {
	    return this->id.name;
	}

        /** Set title */
        void set_title(const std::string& t) {
            this->id.title = t;
        }

        /** Get title */
        const std::string & get_title() const {
            return this->id.title;
        }

	//! Add new annotation
	void add_annotation( const std::string & key, const std::string & value ) {
	    this->setAnnotation( key, value );
	}

	/** Set annotations */
	void set_annotations(const std::vector< std::pair< std::string, std::string > > & ann ) {
	    for (auto a: ann) {
	        this->setAnnotation(a.first, a.second);
	    }
	}

	/** get annotations */
	void get_annotations( std::vector< std::pair< std::string, std::string > > & ann ) const {
	    ann.reserve(this->annotations().size());
	    for (auto a: this->annotations()) {
	        ann.push_back(std::make_pair(a.first, a.second.data()));
	    }
	}

	//! Retrieve the number of entries
	unsigned int get_entries( ) const {
	    return T::entries;
	}

	//! Update the number of entries
	void set_entries( unsigned int entries ) {
	    T::entries = entries;
	}

	/** Retrieve number of axes */
	unsigned int get_axes_number( ) const {
	    return T::axes.size();
	}
        
        /**  Update title of axis */
        void set_axis_title( oh::Axis::ID axis, const std::string & title ) {
            if ( (unsigned int)axis >= T::axes.size() )
                T::axes.resize( axis + 1 );
            T::axes[axis].title = title;
        }

	/** Retrieve title of axis */
	const std::string & get_axis_title( oh::Axis::ID axis ) const {
            assert( (unsigned int)axis < T::axes.size() );
            return T::axes[axis].title;
        }

	/** Get reference to the indices of axes labels */
	std::vector<int> & indices( oh::Axis::ID axis ) {
	    assert( oh::Axis::X <= axis && axis <= oh::Axis::Z );

	    if ( (unsigned int)axis >= T::axes.size() )
	        T::axes.resize( axis + 1 );
	    return T::axes[axis].indices;
	}

	/** Get const reference to the indices of axes labels */
	const std::vector<int> & get_indices( oh::Axis::ID axis ) const {
	    assert( oh::Axis::X <= axis && axis <= oh::Axis::Z );
	    assert( (unsigned int)axis < T::axes.size() );
	    return T::axes[axis].indices;
	}

	/** Returns reference to the array of indices of axes labels.
	 *  \param axis Must be X, Y or Z.
	 *  \return Refernce to the vector of indices.
	 */
	std::vector<std::string> & labels( oh::Axis::ID axis ) {
	    assert( oh::Axis::X <= axis && axis <= oh::Axis::Z );

	    if ( (unsigned int)axis >= T::axes.size() )
	        T::axes.resize( axis + 1 );
	    return T::axes[axis].labels;
	}

	/** Get const reference to the labels of the axes */
	const std::vector<std::string> & get_labels( oh::Axis::ID axis ) const {
	    assert( oh::Axis::X <= axis && axis <= oh::Axis::Z );
	    assert( (unsigned int)axis < T::axes.size() );
	    return T::axes[axis].labels;
	}

	/** Retrieve axis type */
	oh::Axis::Kind get_axis_type( oh::Axis::ID axis ) const {
	    if ( (unsigned int)axis >= T::axes.size() )
	        return oh::Axis::NoAxis;
	    return T::axes[axis].kind;
	}

	/** Retrieve the number of bins for the axis */
	int get_bin_count( oh::Axis::ID axis ) const {
	    if ( (unsigned int)axis < (unsigned int)T::dimension )
	        return T::axes[axis].nbins;
	    else
	        return -1;
	}

	/** Retrieve variable axis partition
	 * This function may only be called if axis_type() returns VariableAxis.
	 * \param axis Must be X, Y or Z.
	 * \return A double std::vector which will be filled with the bin edges.
	 */
	const std::vector<double> & get_axis_range( oh::Axis::ID axis ) const {
	    assert( get_axis_type( axis ) == oh::Axis::Variable );
	    assert( (unsigned int)axis < T::axes.size() );
	    return T::axes[axis].range;
	}

	/** Retrieve a fixed axis partition
	 * This function may only be called if axis_type() returns FixedAxis.
	 * \param axis Must be X, Y or Z.
	 * \param low Lower edge of the first bin.
	 * \param width Bin width.
	 */
	void get_axis_range( oh::Axis::ID axis, double & low, double & width ) const {
	    assert( get_axis_type( axis ) == oh::Axis::Fixed );

	    low   = T::axes[axis].range[ 0 ];
	    width = T::axes[axis].range[ 1 ];
	}

	/** Create a variable axis partition
	 * This function stores bins range for a particular axis and sets axis type to VariableAxis.
	 * \param axis Must be X, Y or Z.
	 * \param xbins A double std::vector of bin edges.
	 */
	void set_axis_range( oh::Axis::ID axis, const std::vector<double> & xbins ) {
	    set_axis_range( axis, &xbins[0], xbins.size() );
	}

	/** Create a variable axis partition
	 * This function stores bins range for a particular axis and sets axis type to VariableAxis.
	 * \param axis Must be X, Y or Z.
	 * \param edges A double arary of bin edges.
	 * \param size Size of the bin edges array.
	 */
	void set_axis_range( oh::Axis::ID axis, const double * edges, unsigned int size ) {
	    if ( (unsigned int)axis >= T::axes.size() )
	        T::axes.resize( axis + 1 );

	    T::axes[axis].kind = oh::Axis::Variable;
	    T::axes[axis].nbins = size - 1;
	    T::axes[axis].range.resize( size );
	    memcpy( &T::axes[axis].range[0], edges, sizeof( double ) * size );
	}

	/** Create a fixed axis partition
	 * This function sets axis type to FixedAxis.
	 * \param axis Must be X, Y or Z.
	 * \param low Lower edge of the first bin.
	 * \param width Bin width.
	 * \param nbins Number of bins.
	 */
	void set_axis_range( oh::Axis::ID axis, double low, double width, unsigned int nbins ) {
	    if ( (unsigned int)axis >= T::axes.size() )
	        T::axes.resize( axis + 1 );

	    T::axes[axis].kind = oh::Axis::Fixed;
	    T::axes[axis].nbins = nbins;
	    T::axes[axis].range.resize( 2 );
	    T::axes[axis].range[0] = low;
	    T::axes[axis].range[1] = width;
	}
    };
}

#endif // OH_CORE_OBJECT_BASE_H

