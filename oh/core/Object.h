#ifndef OBJECT_H
#define OBJECT_H

#include <is/info.h>

#include "oh/core/Identity.h"
#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>

namespace oh
{
/**
 * This class is used to distinguish OH objects from any other IS ones
 * 
 * @author  produced by the IS generator
 */

class Object : public ISInfo {
public:

    /**
     * Object identity
     */
    Identity                                   id;


    static const ISType & type() {
	static const ISType type_ = Object( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "id: " << id << "\t// Object identity";
	return out;
    }

    Object( )
      : ISInfo( "Object" )
    {
	initialize();
    }

    ~Object(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    Object( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << id;
    }

    void refreshGuts( ISistream & in ){
	in >> id;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const Object & info ) {
    info.print( out );
    return out;
}

}

#endif // OBJECT_H
