#ifndef EFFICIENCY_H
#define EFFICIENCY_H

#include "oh/core/Object.h"
#include "oh/core/HistogramData.h"
#include <string>
#include <ostream>
#include <vector>


// <<BeginUserCode>>

// <<EndUserCode>>

namespace oh
{
/**
 * 
 * @author  produced by the IS generator
 */

class Efficiency : public Object {
public:

    /**
     * Global parameter for prior beta distribution
     */
    double                            betaAlpha;

    /**
     * Global parameter for prior beta distribution
     */
    double                            betaBeta;

    /**
     * Per bin parameters for prior beta distribution
     */
    std::vector<double>               betaAlphaPerBin;

    /**
     * Per bin parameters for prior beta distribution
     */
    std::vector<double>               betaBetaPerBin;

    /**
     * Confidence level
     */
    double                            confidenceLevel;

    /**
     * Weight for all events
     */
    double                            weight;

    /**
     */
    HistogramData<double>             total;

    /**
     */
    HistogramData<double>             passed;


    static const ISType & type() {
	static const ISType type_ = Efficiency( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	Object::print( out );
	out << std::endl;
	out << "betaAlpha: " << betaAlpha << "\t// Global parameter for prior beta distribution" << std::endl;
	out << "betaBeta: " << betaBeta << "\t// Global parameter for prior beta distribution" << std::endl;
	out << "betaAlphaPerBin[" << betaAlphaPerBin.size() << "]:\t// Per bin parameters for prior beta distribution" << std::endl;
	for ( size_t i = 0; i < betaAlphaPerBin.size(); ++i )
	    out << i << " : " << betaAlphaPerBin[i] << std::endl;
	out << "betaBetaPerBin[" << betaBetaPerBin.size() << "]:\t// Per bin parameters for prior beta distribution" << std::endl;
	for ( size_t i = 0; i < betaBetaPerBin.size(); ++i )
	    out << i << " : " << betaBetaPerBin[i] << std::endl;
	out << "confidenceLevel: " << confidenceLevel << "\t// Confidence level" << std::endl;
	out << "weight: " << weight << "\t// Weight for all events" << std::endl;
	out << "total: " << total << "\t// " << std::endl;
	out << "passed: " << passed << "\t// ";
	return out;
    }

    Efficiency( )
      : Object( "Efficiency" )
    {
	initialize();
    }

    ~Efficiency(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    Efficiency( const std::string & type )
      : Object( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	Object::publishGuts( out );
	out << betaAlpha << betaBeta << betaAlphaPerBin << betaBetaPerBin << confidenceLevel;
	out << weight << total << passed;
    }

    void refreshGuts( ISistream & in ){
	Object::refreshGuts( in );
	in >> betaAlpha >> betaBeta >> betaAlphaPerBin >> betaBetaPerBin >> confidenceLevel;
	in >> weight >> total >> passed;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const Efficiency & info ) {
    info.print( out );
    return out;
}

}

#endif // EFFICIENCY_H
