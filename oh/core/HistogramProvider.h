#ifndef HISTOGRAMPROVIDER_H
#define HISTOGRAMPROVIDER_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>

namespace oh
{
/**
 * Each histogram provider application is represented in IS by the object of this type
 * 
 * @author  produced by the IS generator
 */

class HistogramProvider : public ISInfo {
public:

    /**
     * True when the provider is up and running, false otherwise.
     */
    bool                                       active;


    static const ISType & type() {
	static const ISType type_ = HistogramProvider( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "active: " << active << "\t// True when the provider is up and running, false otherwise.";
	return out;
    }

    HistogramProvider( )
      : ISInfo( "HistogramProvider" )
    {
	initialize();
    }

    ~HistogramProvider(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    HistogramProvider( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << active;
    }

    void refreshGuts( ISistream & in ){
	in >> active;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const HistogramProvider & info ) {
    info.print( out );
    return out;
}

}

#endif // HISTOGRAMPROVIDER_H
