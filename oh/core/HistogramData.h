/*
 oh::HistogramData.h - OH private header file.

 Fredrik Sandin, Oct 2001
 Monika Barczyk, Feb 2003
 Sergei Kolos,   May 2004
 */

#ifndef OH_HISTOGRAM_DATA_H
#define OH_HISTOGRAM_DATA_H

#include <string>
#include <oh/core/IHistogram.h>

//! histogram
/*!
 This class provides an interface to histogram data and acts
 as an abstraction layer to the underlying representation of
 a histogram.
 \sa oh::Histogram
 */

namespace oh {
    template<typename T>
    class HistogramData: public IHistogram {
    public:
        static const ISType& type() {
            static const ISType type_ = oh::HistogramData<T>().ISInfo::type();
            return type_;
        }

        //! Default constructor
        HistogramData(const std::string &cl = ClassName) :
                IHistogram(cl), bins_(0), bins_size_(0), release_(false) {
            ;
        }

        //! Destructor
        ~HistogramData() {
            if (release_)
                delete[] bins_;
        }

        //! Copy constructor
        HistogramData(const HistogramData &hd) :
                IHistogram(hd), bins_size_(hd.bins_size_), release_(true) {
            bins_ = new T[bins_size_];
            memcpy(bins_, hd.bins_, bins_size_ * sizeof(T));
        }

        //! Copy operator
        HistogramData& operator=(const HistogramData &hd) {
            if (this != &hd) {
                this->IHistogram::operator=(hd);
                if (release_) {
                    delete[] bins_;
                }
                bins_size_ = hd.bins_size_;
                bins_ = new T[bins_size_];
                memcpy(bins_, hd.bins_, bins_size_ * sizeof(T));
                release_ = true;
            }
            return *this;
        }

        //! Retrieve the value of a bin
        T get_bin_value(int x, int y, int z) const {
            return bins_[index(x, y, z)];
        }

        //! Update the value of a bin
        void set_bin_value(int x, int y, int z, T value) {
            alloc_bins();
            bins_[index(x, y, z)] = value;
        }

        //! Retrieve the pointer to the array of bin values
        T* get_bins_array(bool orphan = false) const {
            release_ = !orphan;
            return bins_;
        }

        //! Retrieve the size of the array of bin values
        size_t get_bins_size() const {
            return bins_size_;
        }

        //! Set the pointer to the array of bin values to array. This function does not copy the array.
        void set_bins_array(T *array, unsigned int size, bool release = false) {
            if (release_)
                delete[] bins_;
            bins_size_ = size;
            bins_ = array;
            release_ = release;
        }

        void set_bins_array( const T * array, unsigned int size ) {
            if ( release_ )
                delete[] bins_;
            bins_size_ = size;
            bins_ = const_cast<T*>( array );
            release_ = false;
        }

    protected:
        void publishGuts(ISostream &out) {
            Histogram::publishGuts(out);
            out.put(bins_, bins_size_);
        }

        void refreshGuts(ISistream &in) {
            Histogram::refreshGuts(in);
            if (release_)
                delete[] bins_;
            in.get(&bins_, bins_size_);
            release_ = true;
        }

    private:
        // Allocates memory for bin heights
        void alloc_bins() {
            if (bins_size_ == 0) {
                bins_size_ = (get_bin_count(oh::Axis::X) + 2)
                        * (get_bin_count(oh::Axis::Y) + 2)
                        * (get_bin_count(oh::Axis::Z) + 2);
                bins_ = new T[bins_size_];
                release_ = true;
            }
        }

    private:

        T *bins_;
        size_t bins_size_;
        mutable bool release_;

        static std::string ClassName;
    };
}

#endif // OH_HISTOGRAM_DATA_H
