#ifndef IHISTOGRAM_H
#define IHISTOGRAM_H

#include <oh/core/ObjectBase.h>
#include "oh/core/Histogram.h"

namespace oh
{
/**
 * This class defines an interface for histogram objects stored in IS
 * 
 */

class IHistogram : public ObjectBase<Histogram> {
public:
    IHistogram() = default;

    void set_dimension( oh::Histogram::Dimension d ) {
        dimension = d;
    }

    oh::Histogram::Dimension get_dimension( ) const {
        return dimension;
    }

    double get_error( int x, int y, int z ) const {
        assert( errors.size() != 0 );
        return errors[ index( x, y, z ) ];
    }

    const double * get_errors( ) const {
        return ( errors.size() > 0 ? &errors[0] : 0 );
    }

    void set_error( int x, int y, int z, double error ) {
        alloc_errors( );
        errors[ index( x, y, z ) ] = error;
    }

    void set_errors( double * errors, unsigned int size ) {
        this->errors.resize( size );
        memcpy( &(this->errors[0]), errors, size * sizeof( double ) );
    }

protected:
    IHistogram( const std::string & type )
      : ObjectBase<Histogram>( type )
    { }

    int index( int x, int y, int z ) const {
        int wx = get_bin_count( oh::Axis::X ) + 2;
        int wy = get_bin_count( oh::Axis::Y ) + 2;
        return x + wx * ( y + wy * z );
    }

    void alloc_errors( ) {
        if ( errors.size() == 0 ) {
            int size = ( get_bin_count( oh::Axis::X ) + 2 ) *
                       ( get_bin_count( oh::Axis::Y ) + 2 ) *
                       ( get_bin_count( oh::Axis::Z ) + 2 );
            errors.resize( size );
        }
    }
};
}

#endif // HISTOGRAM_H
