#ifndef OH_FILE_SAVER_H
#define OH_FILE_SAVER_H

#include <memory>
#include <sstream>

#include <TFile.h>
#include <TKey.h>
#include <oh/OHRootReceiver.h>

class OHFileSaver: public OHRootReceiver {
public:
    OHFileSaver(const std::string &file_name, bool remove_policy = false, int compression_level = 0) :
            file_(TFile::Open(file_name.c_str(), "RECREATE")), error_(false), remove_(remove_policy) {
        if (file_.get() == 0) {
            error_ = true;
        } else {
            file_.get()->SetCompressionLevel(compression_level);
        }
    }

    ~OHFileSaver() {
        if (!error_) {
            file_->Close();
        }
    }

    const char* name() {
        if (error_)
            return "";
        else
            return file_->GetName();
    }

    void useOfflineStyle(int run_number) {
        std::ostringstream out;
        out << "run_" << run_number;
        run_dir_ = out.str();
    }

    bool error() const {
        return error_;
    }

    void pushd(const std::string &directory) const {
        std::string name = validate(directory);
        TDirectory *dir = (TDirectory*) gDirectory->FindObject(name.c_str());
        if (!dir) {
            dir = gDirectory->mkdir(name.c_str());
        }
        dir->cd();
    }

    void popd() const {
        gDirectory->cd("..");
    }

private:

    std::string validate(const std::string &name) const {
        std::string result = name;
        replace(result.begin(), result.end(), ':', '_');
        return result;
    }

    void write(const char *name, TNamed *object, int tag) const {
        int begin = 0;
        while (name[begin] == '/' || name[begin] == '.')
            begin++;

        for (int i = begin; name[i] != 0; ++i) {
            if (name[i] == '/' || name[i] == '.') {
                std::string dir(name, begin, i - begin);
                pushd(dir);
                write(name + i + 1, object, tag);
                popd();
                return;
            }
        }

        object->SetName(name);
        if (tag >= 0 && run_dir_.empty()) {
            TKey *key = file_->CreateKey( gDirectory, object, name, gDirectory->GetBufferSize());
            key->WriteFile(tag, file_.get());
        } else {
            object->Write(name);
        }
    }

    void save_object(TNamed *object, int tag) const {
        if (error_) {
            return;
        }

        std::string name = object->GetName();
        if (!run_dir_.empty()) {
            pushd(run_dir_);

            char lb[32];
            sprintf(lb, "lb_%d", tag);
            pushd(lb);

            write(name.c_str(), object, tag);

            popd();
            popd();
        } else {
            write(name.c_str(), object, tag);
        }
    }

    void receive(OHRootHistogram &hh) {
        save_object(hh.histogram.get(), hh.tag);
    }

    void receive(std::vector<OHRootHistogram*> &v) {
        for (unsigned int i = 0; i < v.size(); i++) {
            save_object(v[i]->histogram.get(), v[i]->tag);
        }
    }

    void receive(OHRootEfficiency &eff) {
        save_object(eff.efficiency.get(), eff.tag);
    }

    void receive(std::vector<OHRootEfficiency*> &v) {
        for (unsigned int i = 0; i < v.size(); i++) {
            save_object(v[i]->efficiency.get(), v[i]->tag);
        }
    }

    void receive(OHRootGraph &hh) {
        save_object(hh.graph.get(), hh.tag);
    }

    void receive(std::vector<OHRootGraph*> &v) {
        for (unsigned int i = 0; i < v.size(); i++) {
            save_object(v[i]->graph.get(), v[i]->tag);
        }
    }

    void receive(OHRootGraph2D &hh) {
        save_object(hh.graph.get(), hh.tag);
    }

    void receive(std::vector<OHRootGraph2D*> &v) {
        for (unsigned int i = 0; i < v.size(); i++) {
            save_object(v[i]->graph.get(), v[i]->tag);
        }
    }

    bool removePolicy() {
        return remove_;
    }

private:
    std::unique_ptr<TFile> file_;
    bool error_;
    bool remove_;
    std::string run_dir_;
};

#endif
