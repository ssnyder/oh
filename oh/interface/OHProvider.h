/*
    OHProvider.h - OH private header file.

    OH histogram provider baseclass definition.
    Fredrik Sandin, Oct 2001
    Monika Barczyk, Sept 2002
    Serguei Kolos, Jan 2004
    Herbert Kaiser, Aug 2006
*/

#ifndef OH_HISTOGRAM_PROVIDER_H
#define OH_HISTOGRAM_PROVIDER_H

#include <oh/OHCommandListener.h>
#include <oh/core/Histogram.h>
#include <oh/core/Graph.h>
#include <oh/exceptions.h>

namespace oh
{
  class  ProviderManager;
}

//! Histogram Provider baseclass
/*!
    This class provides functionality to publish the OH internal histogram and graph
    types and thus acts as an abstraction layer to the underlying technology
    used to transport and buffer histograms/graphs. Derive a new class for each
    external package (or histogram format) which should export histograms/graphs
    to the OH.

    Symbolic example:

    \code
    class MyProvider : public OHProvider {
    public:
        MyProvider( const IPCPartition & p,
                    const string & server,
                    const string & name ) :
                    OHProvider( p, server, name ) {
        }

        void publish( const MyHistogram1D& h, const string & name, int tag ) {
            oh::HistogramData<bin type> data;
            // Copy content from h to data
            data.set_title( h.title( ) );

            // More conversion stuff goes here...

            // Publish the histogram
            OHProvider::publish( data, name, tag );
        }

        // Here some functions for other histogram types
        // could be added.

    };
    \endcode

    For more information on how to convert to the OH internal histogram
    representations see oh::HistogramData<T> class. For the OH internal graphs
    there is an interface class called oh::GraphInterface which provides the same
    functionality.

    \sa OHRootProvider and OHRawProvider
*/
class OHProvider : public ISCommandListener
{
  friend class oh::ProviderManager;
  
  public:

    virtual ~OHProvider( );
    
    const std::string & getName() const { return name_; }
    
    const std::string & getServerName() const { return server_; }
    
    const IPCPartition & getPartition() const { return partition_; }    
    
  protected:

    //! Create a new histogram provider
    OHProvider( const IPCPartition & p,
		const std::string & server,
		const std::string & name,
		OHCommandListener * lst = 0 );

    //! Publish histogram
    void publish( ISInfo & object, const std::string & name ) const;
    
    //! Publish histogram
    void publish( ISInfo & object, const std::string & name, int tag ) const;
    
  private:

    void command( const std::string & name, const std::string & cmd );
        
    bool		valid_;
    IPCPartition	partition_;
    std::string		server_;
    std::string		name_;
    OHCommandListener * listener_;
};

#endif // OH_HISTOGRAMPROVIDER_H
