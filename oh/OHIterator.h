/*
    OHIterator.h - OH Public header file.
    
    Serguei Kolos,  Apr 2006
*/

#ifndef OH_ITERATOR_H
#define OH_ITERATOR_H

#include <oh/core/Efficiency.h>
#include <oh/core/Histogram.h>
#include <oh/core/Graph.h>
#include <oh/core/Graph2D.h>

#include <oh/internal/iterator.h>


//! OH objects iterator
/*!
    \class OHIterator
    This is a \b typedef for oh::iterator class which can be used to select, 
    list and retrieve any objects from the OH.
*/
typedef oh::iterator<oh::Object>	OHIterator;

//! OH histograms iterator
/*!
    \class OHHistogramIterator
    This is a \b typedef for oh::iterator class which can be used to select, 
    list and retrieve only histograms from the OH.
*/
typedef oh::iterator<oh::Histogram>	OHHistogramIterator;

//! OH efficiency objects iterator
/*!
    \class OHEfficiencyIterator
    This is a \b typedef for oh::iterator class which can be used to select,
    list and retrieve only efficiency objects from the OH.
*/
typedef oh::iterator<oh::Efficiency>     OHEfficiencyIterator;

//! OH graphs iterator
/*!
    \class OHGraphIterator
    This is a \b typedef for oh::iterator class which can be used to select, 
    list and retrieve only graphs from the OH.
*/
typedef oh::iterator<oh::Graph>		OHGraphIterator;

//! OH 1D graphs iterator
/*!
    \class OHGraph1DIterator
    This is a \b typedef for oh::iterator class which can be used to select, 
    list and retrieve only 1-dimensional graphs from the OH.
*/
typedef oh::iterator<oh::Graph,true>	OHGraph1DIterator;

//! OH 2D graphs iterator
/*!
    \class OHGraph2DIterator
    This is a \b typedef for oh::iterator class which can be used to select, 
    list and retrieve only 2-dimensional graphs from the OH.
*/
typedef oh::iterator<oh::Graph2D,true>	OHGraph2DIterator;

//! OH efficiency iterator
/*!
    \class OHEfficiencyIterator
    This is a \b typedef for oh::iterator class which can be used to select,
    list and retrieve only graphs from the OH.
*/
typedef oh::iterator<oh::Efficiency>         OHEfficiencyIterator;

#endif
