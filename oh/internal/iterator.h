/*
    iterator.h - OH Public header file.
    
    Serguei Kolos,  Dec 2006
*/

#ifndef OH_INTERNAL_ITERATOR_H
#define OH_INTERNAL_ITERATOR_H

#include <is/infoiterator.h>

#include <oh/interface/OHReceiver.h>
#include <oh/OHUtil.h>

/*! \namespace oh
 *  This is a wrapping namespace for internal OH classes.
 */
namespace oh
{

    /*! \brief This class provides a possibility to iterate through a selected sub-set of objects
        from the OH. It can be configured to work with specific type of such
        objects i.e. with only histograms or only graphs.
        There are several names generated of this template by \c typedef:
        OHIterator, OHHistogramIterator, OHEfficiencyIterator, OHGraphIterator, OHGraph1DIterator, OHGraph2DIterator
    */
    template <class T, bool STRICT = false>
    class iterator : public ISInfoIterator
    {
    public:

	/*!
	    Constructs a new iterator for OH objects.
	    Immediately after construction the "current object" is undefined and
	    the iterator must be advanced with one of the ++ operators.

	    \param partition A valid IPCPartition
	    \param server Name of a valid OH server
	    \param provider Optional name of histogram provider, should only contain
	    [a-z], [A-Z], [0-9], '_' and optional wildcars according to the Posix
	    style of regular expressions
	    \param object Optional name of histogram, should only contain
	    [a-z], [A-Z], [0-9], '_' and optional wildcars according to the Posix
	    style of regular expressions
	*/
	iterator( const IPCPartition & partition,
		  const std::string & server,
		  const std::string & provider = ".*",
		  const std::string & object = ".*" ) 
        try : ISInfoIterator(  partition, server, 
          		    ( STRICT ? T::type() : ~T::type() ) && oh::util::create_regex( provider, object ) )
        { ; } 
        catch ( daq::is::InvalidCriteria & ex ) {
            throw daq::oh::InvalidRegex( ex.context(), ex.get_expression() );
        }
        catch ( daq::is::RepositoryNotFound & ex ) {
            throw daq::oh::RepositoryNotFound( ex.context(), ex.get_name() );
        }

	/*!
	    Retrieves the object or set of objects at the current iterator's position.
            \param receiver An instance of an OHReceiver derived class that. The object at the
            current iterator position will be passed to this receiver via one of its virtual functions.
	    \param with_history If false the last value of the object will be retrieved.
            If true the given number of the history values will be retrieved.
	    \param how_many If with_history parameter is set to true then this number defines how many
	        versions of the current object to read, -1 means all.
	*/
	void retrieve( OHReceiver & receiver, bool with_history = false, short how_many = -1 ) 
        							 {
	    receiver.readObjects( *this, with_history, how_many );
        }

	/*!
	    Retrieves a version of the current object that corresponds to the given tag
            \param receiver An instance of an OHReceiver derived class that. The object at the
            current iterator position will be passed to this receiver via one of its virtual functions.
	    \param tag the version of the object to be read.
	*/
	void retrieve( OHReceiver & receiver, int tag ) {
	    receiver.readObjectWithTag( *this, tag );
	}

	/*!
	    Returns the name of the provider of the object at the current iterator's position.
	    \return The name of the provider that published the object at the current position
	    of this iterator. Returns an empty string if the current position is undefined.
	*/
	std::string provider( ) const {
	    return oh::util::get_provider_name( ISInfoIterator::name( ) );
	}

	/*!
	    Exercises the type of the object at the current iterator position
	    \return true if the current object is efficiency
	*/
	bool isEfficiency( ) const {
	    return ( type() == oh::Efficiency::type() );
	}

        /*!
            Exercises the type of the object at the current iterator position
            \return true if the current object is histogram
        */
        bool isHistogram( ) const {
            return ( type().subTypeOf( oh::Histogram::type() ) );
        }

	/*!
	    Exercises the type of the object at the current iterator position
	    \return true if the current object is graph
	*/
	bool isGraph( ) const {
	    return ( type() == oh::Graph::type() );
	}

	/*!
	    Exercises the type of the object at the current iterator position
	    \return true if the current object is 2D graph
	*/
	bool isGraph2D( ) const {
	    return ( type() == oh::Graph2D::type() );
	}

	/*!
	    Returns the name of the object at the current iterator's position.
            \return The name of the object at the current position
            of this iterator. Returns an empty string if the current position is undefined.
	*/
	std::string name( ) const {
	    return oh::util::get_object_name( ISInfoIterator::name( ) );
	}
    };
}

#endif
