/*
    OHProviderManager.h - OH private header file.

    OH histogram provider manager definition.
    Serguei Kolos, Jan 2006
*/

#ifndef OH_PROVIDER_MANAGER_H
#define OH_PROVIDER_MANAGER_H

#include <list>
#include <map>
#include <mutex>

#include <ipc/alarm.h>
#include <ipc/singleton.h>
#include <is/infoprovider.h>
#include <is/infodictionary.h>
#include <oh/exceptions.h>
#include <oh/interface/OHProvider.h>

namespace oh
{
    class provider
    {
     public:
	provider( const IPCPartition & p,
		  const std::string & server,
		  const std::string & name );
        
        ~provider();
        
        void addListener( OHCommandListener * oh_lst, ISCommandListener * is_lst );
	
        void removeListener( OHCommandListener * rec, ISCommandListener * is_lst );
        
        bool active() const { return !listeners_.empty(); }
        
     private:
     	provider( const provider & );
     	provider & operator=( const provider & );
        
     private:
	bool checkAndRepublish();
        
     private:
        struct listener
        {
            listener( OHCommandListener * ol, ISCommandListener * il = 0, bool av = false )
              : ohl( ol ),
                isl( il ),
                active( av )
            { ; }

	    bool
	    operator==( const listener & x ) const
	    { return (	   ( ohl == 0 || x.ohl == 0 || ohl == x.ohl )
            		&& ( isl == 0 || x.isl == 0 || isl == x.isl ) ); }
              
            OHCommandListener * ohl;
            ISCommandListener * isl;
            bool		active;
        };
        
        typedef std::list<listener> Listeners;
        
     private:
	std::mutex	 mutex_;
        ISInfoDictionary dictionary_;
	std::string	 name_;
        Listeners	 listeners_;
        IPCAlarm	 alarm_;
    };
    
    class provider_manager
    {
     public:
	friend class IPCSingleton<oh::provider_manager>;
        
	void
	registerProvider(	const IPCPartition & p,
        			const std::string & server,
                        	const std::string & name,
                        	OHCommandListener * oh_lst,
                                ISCommandListener * is_lst );

	void
	unregisterProvider(	const IPCPartition & p,
        			const std::string & server,
                        	const std::string & name,
                        	OHCommandListener * oh_lst,
                                ISCommandListener * is_lst );

	void destroy();
        
     private:
     	provider_manager() { ; }
        
        provider_manager( const provider_manager & );
     	provider_manager & operator=( const provider_manager & );
        
     private:
	typedef std::map<std::string,provider*>   Providers;
        
	std::mutex	mutex_;
        Providers	providers_;
    };
}

typedef IPCSingleton<oh::provider_manager> OHProviderManager;

#endif
