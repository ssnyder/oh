#ifndef OH_EXCEPTIONS_H
#define OH_EXCEPTIONS_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      oh/exceptions.h
//
//      private header file for the IS library
//
//      Sergei Kolos March 2004
//
//      description:
//              Declares IS exceptions
//////////////////////////////////////////////////////////////////////////////////////

#include <ers/ers.h>

/*! \namespace daq
 *  This is a wrapping namespace for all OH exceptions.
 */ 
namespace daq {
    /*! \namespace daq::oh
     *  This is a wrapping namespace for all OH exceptions and internal utilities.
     */
  
    /*! \class oh::Exception
     *  This is a base class for all IS exceptions.
     */
    ERS_DECLARE_ISSUE( oh, Exception, ERS_EMPTY, ERS_EMPTY )
                                                                                                                                          
    /*! \class oh::NotFound
     *  Base class for the daq::oh::XXXNotFound exceptions.
     */
    ERS_DECLARE_ISSUE_BASE( oh, NotFound, oh::Exception, ERS_EMPTY, ERS_EMPTY, ERS_EMPTY )
    
    /*! \class oh::ProviderAlreadyExist
     *	This issue is reported if one tries to create an OH provider which already registered with the OH repository.
     */
    ERS_DECLARE_ISSUE_BASE(	oh,
				ProviderAlreadyExist,
				oh::Exception,
				"OH Provider with the '" << name << "' name already registred with the '" 
                                << server << "' OH repository.",
				ERS_EMPTY,
				((std::string)name )
				((std::string)server )
			)
    
    /*! \class oh::InvalidRegex
     *	This issue is reported if one tries to use badly formed regular expression as input parameter for one of the OH functions.
     */
    ERS_DECLARE_ISSUE_BASE(	oh,
				InvalidRegex,
				oh::Exception,
				"Invalid regular expression '" << regex << "' has been used '",
				ERS_EMPTY,
				((std::string)regex )
			)
    
    /*! \class oh::ObjectTypeMismatch
     *	This issue is reported if one tries to publish an OH object (e.g. histogram) using a name
     *  which is already taken by the object of different type (e.g. graph).
     */
    ERS_DECLARE_ISSUE_BASE(	oh,
				ObjectTypeMismatch,
				oh::Exception,
				"The '" << name << "' name is already taken by the object of different type",
				ERS_EMPTY,
				((std::string)name )
			)
    
    /*! \class oh::ObjectNotFound
     *	This issue is reported if one tries to create an OH objects (graph or histogram) which already exists in the OH repository.
     */
    ERS_DECLARE_ISSUE_BASE(	oh,
				ObjectNotFound,
				oh::NotFound,
				"Object '" << name << "' provided by the '" 
                                		<< provider << "' with the '" << tag << "' tag does not exist in the '"
                                                << server << "' OH repository",
				ERS_EMPTY,
				((std::string)server )
				((std::string)provider )
				((std::string)name )
				((int)tag )
			)
    
    /*! \class oh::RepositoryNotFound
     *  This issue is reported if a particular OH repository is not available.
     */
    ERS_DECLARE_ISSUE_BASE(     oh,
                                RepositoryNotFound,
                                oh::NotFound,
                                "OH repository '" << name << "' does not exist",
                                ERS_EMPTY,
                                ((std::string)name )
                        )

    /*! \class oh::ProviderNotFound
     *  This issue is reported if an OH provider does not exist.
     */
    ERS_DECLARE_ISSUE_BASE(     oh,
                                ProviderNotFound,
                                oh::NotFound,
                                "OH provider '" << name << "' does not exist",
                                ERS_EMPTY,
                                ((std::string)name )
                        )
                                                                                                                                          
    /*! \class oh::InvalidObject
     *	This issue is reported if one tries to publish something which is not a supported by the OH.
     */
    ERS_DECLARE_ISSUE_BASE(	oh,
				InvalidObject,
				oh::Exception,
				"Objects of the given type are not supported by the OH",
				ERS_EMPTY,
			)

    /*! \class oh::InvalidParameter
     *	This issue is reported by OH utilities in case of invalid command line parameter.
     */
    ERS_DECLARE_ISSUE_BASE(	oh,
				InvalidParameter,
				oh::Exception,
				"Invlaid value '" << value << "' was given to the '-t' command line parameter",
				ERS_EMPTY,
				((char)value )
			)
}


#endif // IS_EXCEPTIONS_H
