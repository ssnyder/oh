/*
    OHRawProvider.h - OH Public header file.
    
    Fredrik Sandin, Nov 2001
    Serguei Kolos,  Nov 2004
    Herbert Kaiser, Aug 2006
*/

#ifndef OH_RAWPROVIDER_H
#define OH_RAWPROVIDER_H

#include <vector>
#include <string>

using std::pair;
using std::string;
using std::vector;

#include <oh/interface/OHProvider.h>
#include <oh/core/Graph.h>
#include <oh/core/Graph2D.h>
#include <oh/OHUtil.h>

//! RAW Provider default map
/*!
    This is the default binmap used by the OHRawProvider template,
    it assumes that the bins are ordered according to
    
    index = x + width_x * ( y + width_y * z )
    
    Where z=0 for 2D histograms and y=z=0 for 1D histograms. width_x and
    width_y is the total number (possibly including underflow and overflow)
    of bins in the x and y directions.
    
    x, y and z starts at 0 and end at width_? - 1.
    
    If underflow and overflow bins are used they are addressed with
    minimum and maximum values of x, y and z.
    
    Confused? The idea here is that the OH says: I want the height (or error)
    corresponding to bin (x,y,z) from Your array, where is it? The functions
    in the map should return the correct array index corresponding to (x,y,z).
    
    Observe the possibility to have an array of both bin contents and
    bin errors in a mixed order by providing the appropriate map
    to OHRawProvider
    
    Example:
    
    Lets say we have the bin contents and errors structured as
    
    height1 | error1 | height2 | error2 | ...
    
    in one and the same array of float values. Then we implement a new map
    
    \code
    class MyBins {
    public:

        static int index( long x,   long wx=0,
			   int y=0, long wy=0,
			   int z=0, long wz=0 ) const {
            return ( x + wx * ( y + wy * z ) ) * 2;
        }
    
    \endcode
    
    And then we create the provider object according to (assume float values
    for the axis)
    
    \code
    OHRawProviderDM<float,float,float,MyBins> myprovider;
    myprovider.publish( ... );
    \endcode
    
    In this way there is in principle no limitations to how you have the
    data structured.
    
    \sa OHRawProvider
*/
class OHBins
{
public:

    //! Calculate array index for bins and errors arrays
    static int index(	long x, long wx = 0, long y = 0, long wy = 0, long z = 0 )
    { return x + wx * ( y + wy * z ); }
};

template< class > class OHRawProvider;
class OHRawGraphProvider;

class OHAxis
{
  template< class > friend class OHRawProvider;
  
  public:
    template <typename T>
    OHAxis( const std::string & label, size_t bincount, T low, T width );
    template <typename T>
    OHAxis( const std::string & label, size_t bincount, T * bins );
    
  private:
    std::string		label_;
    size_t		bincount_;
    vector<double>	bins_;
    double		low_;
    double		width_;
};


template <typename T>
inline OHAxis
OHMakeAxis( const std::string & label, size_t bincount, T low, T width )
{
    return OHAxis( label, bincount, low, width );
}

template <typename T>
inline OHAxis
OHMakeAxis( const std::string & label, size_t bincount, T * bins )
{
    return OHAxis( label, bincount, bins );
}


//! Provider of 'RAW' histograms and graphs
/*!
    This template provides functionality to export histograms represented by
    arrays of some fundamental data type to the OH. Graphs with \c double values
    can also be exported.
    The meaning of the template parameters are as follows
    
    TC: The data type used by user to represent bin contents.
    
    TE: The data type used by user to represent bin errors.
    
    TA: The data type used by user to represent axis partitions.
    
    TB: Governs how the user bins are accessed.
    see OHBins (optional).
    
    \sa OHBins
    \sa OHRootProvider

    \todo Support 3D histograms if necessary.
*/
template<class TB = OHBins>
class OHRawProvider : public OHProvider
{
  public:

    //! Create a new Raw provider
    /*!
    \param p		A valid IPCPartition
    \param server	Name of a valid OH server
    \param name		Name of the provider, user is responsible for specifying a
			unique name. Should only contain characters from [a-z], 
                        [A-Z], [0-9] and '_'.
    */
    OHRawProvider( const IPCPartition & p,
                   const string & server,
                   const string & name,
		   OHCommandListener * lst = 0 );

    //! Publish a 1D histogram with fixed width bins
    /*!
    \param name 	A name describing the histogram. Should only contain characters
    				from [a-z], [A-Z], [0-9] and '_'.
    \param title 	The histogram title
    \param xaxis 	X Axis description
    \param bins 	A pointer to the position where the bin contents (heights)
    			are located.
    \param errors 	A pointer to the position where the bin errors are located
    			or 0 if no errors.
    \param hasOverflowAndUnderflow The contents and errors arrays contain overflow
    			and underflow bins.
    \param tag 		Use this tag for the published histogram.
    \param annotations	Annotations that should be attached to the histogram.
    \throw daq::oh::Exception 
    
    \sa OHBins
    */
    template<class TC,class TE>
    void 
    publish(	const string & name,
		const string & title,
		const OHAxis & xaxis,
		const TC * bins,
		const TE * errors,
		bool hasOverflowAndUnderflow,
		int tag = -1,
		const vector< pair<string,string> > & annotations = oh::util::EmptyAnnotation ) const;

    //! Publish a 2D histogram with fixed width bins
    /*!
    \param name 	A name describing the histogram. Should only contain characters
			from [a-z], [A-Z], [0-9] and '_'.
    \param title	The histogram title
    \param xaxis	X Axis description
    \param yaxis	Y Axis description
    \param bins		A pointer to the position where the bin contents (heights) are located.
    \param errors	A pointer to the position where the bin errors are located or 0 if no errors.
    \param hasOverflowAndUnderflow The contents and errors arrays contain overflow and underflow bins.
    \param tag		Use this tag for the published histogram.
    \param annotations	Annotations that should be attached to the histogram.
    \throw daq::oh::Exception 
    
    \sa OHBins
    */
    template<class TC,class TE>
    void
    publish(	const string & name,
		const string & title,
		const OHAxis & xaxis,
		const OHAxis & yaxis,
		const TC * bins,
		const TE * errors,
		bool hasOverflowAndUnderflow,
		int tag = -1,
		const vector< pair<string,string> > & annotations = oh::util::EmptyAnnotation ) const;

    //! Publishes graphs with 2 axes.
    /** Parameters are very similar to the ones for for histograms.
     *  If you want to give errors for the graph points you need to set the error style.
     *  Styles are: \c oh::Graph::NONE for no errors(default)
     *		    \c oh::Graph::SYMMETRIC for ex,ey errors (like in TGraphErrors)
     *		    \c oh::Graph::ASYMMETRIC for exl,exh,eyl,eyh errors (like in TGraphAsymmErrors)
     *		    \c oh::Graph::ASYMBENT for exl,exh,eyl,eyh,exld,exhd,eyld,eylh errors (like in TGraphBentErrors)
     *	The vector for error arrays has to be given in the right order e.g. for \c oh::Graph::SYMMETRIC
     *	you should give the errorlist as follows: \c errorlist[0]=ex; \c errorlist[1]=ey;
     *	Here \c ex and \c ey are double arrays containing the error values.
     */
    void
    publish(	const string & name,
		const string & title,
		unsigned int entries,
		const double * x,
		const double * y,
		const OHAxis & xaxis,
		const OHAxis & yaxis,
		oh::Graph::ErrorStyle estyle=oh::Graph::None,
		const vector<const double *>& errorlist = oh::util::EmptyErrorList,
		int tag = -1,
		const vector< pair<string,string> > & annotations = oh::util::EmptyAnnotation ) const;

    //! Publishes graphs with 3 axes.
    /** Parameters are very similar to the ones for histograms.
     *  If you want to give errors for the graph points you need to set the error style.
     *  Styles are: \c oh::Graph::NONE for no errors(default)
     *		    \c oh::Graph::SYMMETRIC for ex,ey,ez errors (like in TGraphErrors)
     *	The vector for error arrays has to be given in the right order that means for \c oh::Graph::SYMMETRIC
     *	you should give the errorlist as follows: \c errorlist[0]=ex; \c errorlist[1]=ey \c errorlist[2]=ez;
     *	Here \c ex, \c ey and \c ez are double arrays containing the error values.
     */
    void
    publish(	const string & name,
		const string & title,
		unsigned int entries,
		const double * x,
		const double * y,
		const double * z,
		const OHAxis & xaxis,
		const OHAxis & yaxis,
		const OHAxis & zaxis,
		oh::Graph2D::ErrorStyle estyle=oh::Graph2D::None,
		const vector<const double *>& errorlist=oh::util::EmptyErrorList,
		int tag = -1,
		const vector< pair<string,string> > & annotations = oh::util::EmptyAnnotation ) const;
};

#include <oh/OHRawProvider.i>

#endif // OHRAWPROVIDER_H
