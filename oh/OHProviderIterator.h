/*
    OHProviderIterator.h - OH Public header file.
    
    Fredrik Sandin, Oct 2001
    Monika Barczyk, Feb 2003
    Serguei Kolos, Feb 2007
*/

#ifndef OH_PROVIDER_ITERATOR_H
#define OH_PROVIDER_ITERATOR_H

#include <string>
#include <is/infoiterator.h>

//! OH Providers iterator
/*!
    This class provides functionality to retrieve the names of all currently
    active histogram providers which publish histograms in a specific OH server.
*/
class OHProviderIterator : public ISInfoIterator
{
  public:

    //! Create a new provider iterator
    OHProviderIterator( const IPCPartition & p, const std::string & server, const std::string & provider_mask = ".*" );
  
    //! Retrieve name of the current provider
    operator std::string( );
    
    //! Retrieve name of the current provider
    std::string name( );

    //! Returns true if the carrent provider exists, false otherwise
    bool isActive( );
};

#endif
