/*
    OHCommandListener.h - OH private header file.

    OH command receiver baseclass definition.
    Serguei Kolos, Feb 2004
*/
#ifndef OH_COMMAND_LISTENER_H
#define OH_COMMAND_LISTENER_H

#include <string>

//! Receive OH commands
/**
    This class defines an abstract interface for receiving commands which have been sent to histogram providers.
    User has to inherit his class from this one and implement two pure vurtual functions.
    \sa OHCommandSender
 */
class OHCommandListener
{
  public:
    virtual ~OHCommandListener() {;}
    /*!
	This function is called when a command for a particular histogram has been received
	\param histogram_name name of the target histogram
	\param command	received command
     */
    virtual void command( const std::string & histogram_name, const std::string & command ) = 0;
    
    /*!
	This function is called when a command for the provider associated with this
        command listener is received
	\param command	received command
     */
    virtual void command( const std::string & command ) = 0;
};

#endif
